  <?php
if(isset($_SESSION['SERVICE']) || isset($_SESSION['NEWS']) || isset($_SESSION['EVENTS'])) {
  $addRoute = "../../";
} else if(isset($_SESSION['TOPIC'])) {
  $addRoute = "../../../";
} else {
  $addRoute = "";
}
?>
<script src="<?php echo $addRoute;?>js/jquery-3.3.1.min.js"></script>
<script src="<?php echo $addRoute;?>js/jquery-ui.js"></script>
<script src="<?php echo $addRoute;?>js/popper.min.js"></script>
<script src="<?php echo $addRoute;?>js/bootstrap.min.js"></script>
<script src="<?php echo $addRoute;?>js/owl.carousel.min.js"></script>
<script src="<?php echo $addRoute;?>js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo $addRoute;?>js/jquery.sticky.js"></script>
<script src="<?php echo $addRoute;?>js/jquery.waypoints.min.js"></script>
<script src="<?php echo $addRoute;?>js/jquery.animateNumber.min.js"></script>
<script src="<?php echo $addRoute;?>js/aos.js"></script>
<script src="<?php echo $addRoute;?>js/floating.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mobile-detect/1.4.4/mobile-detect.min.js"></script>
<script src="<?php echo $addRoute;?>js/main.js"></script>
<script type="text/javascript">
var md = new MobileDetect(window.navigator.userAgent);
$(document).ready(function(){
    $('#floatMenu').hide();
    if($('#bandera').val()==1){
      $('#service').click();
    }
    if($('#bandera2').val()==1){
      $('#eventos-link').click();
      $('#boletin').removeClass("show active");
      $('#noticias').removeClass("show active");
      $('#eventos').addClass("show active");
    }
    if($('#bandera3').val()==1){
      $('#noticias-link').click();
      $('#boletin').removeClass("show active");
      $('#eventos').removeClass("show active");
      $('#noticias').addClass("show active");
    }
    $('.nav-link').click(function(){
      $("body").removeClass("offcanvas-menu");
    });
    if(md.mobile() !== null){
      $('#floatMenu').hide();
    }
});
$(window).scroll(function(){
  if($('#videoMain').offset().top < $(this).height() + $(this).scrollTop()){
    $('#floatMenu').show();
  }else{
    $('#floatMenu').hide();
  }

  if($('#footer-site').offset().top < $(this).height() + $(this).scrollTop()){
    $('#floatMenu').hide();
  }
  if($('#contact-section').offset().top < $(this).height() + $(this).scrollTop()){
    $('#floatMenu').hide();
  }
  if($('#blog-section').offset().top < $(this).height() + $(this).scrollTop()){
    $('#floatMenu').hide();
  }
  if($('#services-section').offset().top < $(this).height() + $(this).scrollTop()){
    $('#floatMenu').hide();
  }
});
    floatingMenu.add('floatMenu',
        {
            // Represents distance from left or right browser window
            // border depending upon property used. Only one should be
            // specified.
            // targetLeft: 0,
            targetLeft: 0,

            // Represents distance from top or bottom browser window
            // border depending upon property used. Only one should be
            // specified.
            targetTop: 500,
            // targetBottom: 0,

            // Uncomment one of those if you need centering on
            // X- or Y- axis.
            //centerX: true,
            centerY: true,

            // Remove this one if you don't want snap effect
            snap: true
        });
    $('#login').click(function(){
      $('#loginModal').modal('show');
    });
    $('#registro').click(function(){
      $('#registroModal').modal('show');
    });
    $('#registro-slider').click(function(){
      $('#registroModal').modal('show');
    });

    $('#modalTiposParticipacion').click(function(){
      $('#registroModal').modal('hide');
      $('#body-contact').html("");
      $('#title-contact').html("");
      $('#title-contact').html("Tipos de participacion");
      $('#body-contact').html("<h3>NIEVELES DE PARTICIPACION EN CONCIMAZ.</h3><p>La Contraloría Ciudadana de Mazatlán, AC (CONCIMAZ), cuenta con tres mecanismos de participación ciudadana para aportar en el cumplimiento de su Misión, donde te podrás integrar, mismos que se explican a continuación:</p><h4><strong>COMO ASOCIADO.</strong></h4><p>Si deseas integrarte en tu tiempo libre a la CONCIMAZ, para participar en el cumplimiento pleno de su Misión y con derecho a participar en sus órganos directivos o comisiones de trabajo, tu opción es ser miembro Asociado. Se hace mediante el llenado de un formato de solicitud de afiliación dirigida al Consejo Directivo. En un plazo no mayor de cinco días se te dará respuesta a tu solicitud.</p><h4>COMO VOLUNTARIO:</h4> <p>Si solo deseas colaborar sin afiliación en algún momento de tu tiempo libre, podrás hacerlo como Voluntario, donde podrás colaborar activamente en alguna actividad, programa o proyecto de la organización y en los tiempos de que dispongas. O también puedes prestar asesoría, servicio o consulta en el área profesional o de conocimiento que tengas vinculado a nuestra Misión. En el mismo orden, se llena una solicitud de Voluntario. En un plazo no mayor de cinco días se te dará respuesta a tu solicitud.</p> <h4>COMO PATROCINADOR:</h4> <p>Si no puedes participar como Asociado o Voluntario, y deseas aportar algún recurso material, técnico o financiero para el desarrollo de las actividades propias de la Misión de la organización, tu papel es como Patrocinador. En un plazo no mayor de cinco días se te dará respuesta a tu solicitud.</p>");
      $('#alertModal').modal('show').on('hidden.bs.modal', function(e){
        $('#registroModal').modal('show');
      });
    });

    $('#denunciaForm').submit(function(e){
      e.preventDefault();
      var anonimo = null;
      if($('#checkDenuncia').prop('checked')){
        anonimo = 1;
      }else{
        anonimo = 0;
      }
      $.ajax({
        method:'POST',
        url:'actions.php',
        data:{
          "denuncia":true,
          "txtden_nombre": $('#txtden_nombre').val(),
          "txtden_email": $('#txtden_email').val(),
          "txtden_telefono": $('#txtden_telefono').val(),
          "txtden_domicilio": $('#txtden_domicilio').val(),
          "txtden_denunciado": $('#txtden_denunciado').val(),
          "cmbden_ente": $('#cmbden_ente').val()+'-'+$('#cmbden_ente').text(),
          "txtden_puesto": $('#txtden_puesto').val(),
          "dtpden_hechos": $('#dtpden_hechos').val(),
          "txtden_descripcion": $('#txtden_descripcion').val(),
          "anonimo": anonimo
        }
      }).done(function(result){
        if(result === 'enviado'){
          $('#txtden_nombre').val(""),
          $('#txtden_email').val(""),
          $('#txtden_telefono').val(""),
          $('#txtden_domicilio').val(""),
          $('#txtden_denunciado').val(""),
          $('#cmbden_ente').val(0),
          $('#txtden_puesto').val(""),
          $('#dtpden_hechos').val(""),
          $('#txtden_descripcion').val("")
          $('#body-contact').html("");
          $('#title-contact').html("");
          $('#title-contact').html("Denuncias CONCIMAZ");
          $('#body-contact').html("Su denuncia ha sido enviada con exito, gracias por su participacion.");
          $('#alertModal').modal('show');
        }else{
          $('#body-contact').html("");
          $('#title-contact').html("");
          $('#title-contact').html("Denuncias CONCIMAZ");
          $('#body-contact').html("Ha ocurrido un error al enviar su denuncia, intente mas tarde.");
          $('#alertModal').modal('show');
        }
      }).fail(function(xhr, status, error){
        console.log(error)
      });
    });

    $('#contactForm').submit(function(e) {
      e.preventDefault();
      $.ajax({
        method:'POST',
        url:'lib/sendContact.php',
        data:{
          "contact": true,
          "nombres": $('#txtnombres').val(),
          "apellidos": $('#txtapellidos').val(),
          "email": $('#txtemail').val(),
          "message": $('#txtmessage').val()
        },
        success: function(result) {
          if(result.message=='enviado'){
            $('#body-contact').html("");
            $('#title-contact').html("");
            $('#title-contact').html("Contacto CONCIMAZ");
            $('#body-contact').html("Hola, "+$('#txtnombres').val()+" el mensaje ha sido enviado, gracias.");
            $('#alertModal').modal('show');
            //Clean contact form
            $('#txtnombres').val("");
            $('#txtapellidos').val("");
            $('#txtemail').val("");
            $('#txtmessage').val("");
          }else{
            $('#body-contact').html("");
            $('#title-contact').html("");
            $('#title-contact').html("Contacto CONCIMAZ");
            $('#body-contact').html("Ha ocurrido un error al enviar su mensaje, intente mas tarde.");
            $('#alertModal').modal('show');
            //Clean contact form
            $('#txtnombres').val("");
            $('#txtapellidos').val("");
            $('#txtemail').val("");
            $('#txtmessage').val("");
          }
        },
        dataType:'json'
      });
    });

    $("#checkDenuncia").change(function() {
        if(this.checked) {
          $('.personal-data').hide();
          $('#txtden_nombre, #txtden_email, #txtden_telefono, #txtden_domicilio').val("");
        }else{
          $('.personal-data').show();
        }
    });
    function openAbout(option){
      var title = $('#aboutTitle');
      var body = $('#aboutBody');
      var content = [
        {"title":"Misión", "text":"Somos una organización que promueve la participación ciudadana organizada en fomentar la transparencia, rendición de cuentas, combate a la corrupción y la mejora continua de la gestión pública en el municipio de Mazatlán."},
        {"title":"Visión", "text":"Ser una organización civil reconocida por sus logros y aportaciones en materia de transparencia, rendición de cuentas, combate a la corrupción y gobierno abierto en el municipio de Mazatlán."},
        {"title":"Valores", "text":"<ul class='list-group'><li class='list-group-item '>Honestidad</li><li class='list-group-item '>Tolerancia</li><li class='list-group-item '>Respeto</li><li class='list-group-item '>Responsabilidad</li><li class='list-group-item '>Proactividad</li><li class='list-group-item '>Legalidad</li></ul>"},
        {"title":"Mesa direcitiva", "text":`
          <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center" id="headingOne">
                    <h5 class="mb-0">
                      <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><div class="text-left">Dr. José Luis Jorge Figueroa Cancino<br><span style="color: black !important;">Presidente</span></div></a>
                    </h5>
                      <a href="pdf/jorgeFigueroa.pdf" target="_blank"><img src="images/icons/paper.png" style="max-width: 32px;" /></a>
                    </div>
            </div>
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center" id="headingOne">
                <h5 class="mb-0">
                  <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><div class="text-left">Vicente Hernández Delgado<br><span style="color: black !important;">Presidente</span></div></a>
                </h5>
                  <a href="pdf/vicenteHernandez.pdf" target="_blank"><img src="images/icons/paper.png" style="max-width: 32px;" /></a>
                </div>
            </div>
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center" id="headingOne">
                <h5 class="mb-0">
                  <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><div class="text-left">Francisca Nuño González<br><span style="color: black !important;">Tesorera</span></div></a>
                </h5>
                  <a href="pdf/franciscaNuno.pdf" target="_blank"><img src="images/icons/paper.png" style="max-width: 32px;" /></a>
                </div>
            </div>
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center" id="headingOne">
                <h5 class="mb-0">
                  <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><div class="text-left">Gustavo E. Rojo Navarro<br><span style="color: black !important;">Vocal</span></div></a>
                </h5>
                  <a href="#" target="_blank"><img src="images/icons/paper.png" style="max-width: 32px;" /></a>
                </div>
            </div>
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center" id="headingOne">
                <h5 class="mb-0">
                  <a class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><div class="text-left">Efrain Vargas Millan<br><span style="color: black !important;">Vocal</span></div></a>
                </h5>
                  <a href="pdf/efrainVargas.pdf" target="_blank"><img src="images/icons/paper.png" style="max-width: 32px;" /></a>
                </div>
            </div>
          </div>
        `},
        {"title":"Comisiones", "text":"<ul class='list-group'><li class='list-group-item'>Vigilancia</li><li class='list-group-item'>Seguimiento de Obras Públicas</li><li class='list-group-item'>Seguimiento de Servicios Públicos</li><li class='list-group-item'>Comunicación Social y Difusión</li><li class='list-group-item'>Informática y Redes Sociales</li><li class='list-group-item'>Seguimiento y Evaluación de Programas Presupuestarios</li><li class='list-group-item'>Seguimiento de Compras y Adquisiciones</li><li class='list-group-item'>Ética</li><li class='list-group-item'>Asuntos Jurídicos</li><li class='list-group-item'>Comisario</li></ul>"}
      ];
      switch (option) {
        case 'mision':
        title.html(content[0].title);
        body.html(content[0].text);
        $('#modalAbout').modal('show');
        break;
        case 'vision':
        title.html(content[1].title);
        body.html(content[1].text);
        $('#modalAbout').modal('show');
        break;
        case 'valores':
        title.html(content[2].title);
        body.html(content[2].text);
        $('#modalAbout').modal('show');
        break;
        case 'organigrama':
        title.html(content[3].title);
        body.html(content[3].text);
        $('#modalAbout').modal('show');
        break;
        case 'comisiones':
        title.html(content[4].title);
        body.html(content[4].text);
        $('#modalAbout').modal('show');
        break;
        default:
          console.log('Default');
      }
    }
    $('#acercade').hover(function( handlerIn, handlerOut ){
        $(this).off("click");
    });
    function toTop(){
      $("html, body").animate({scrollTop: 0}, 1000);
    }

    function showTab(tab) {
      switch(tab) {
        case 'eventos':
          $('#boletin-tab').removeClass("active");
          $('#noticias-tab').removeClass("active");
          $('#eventos-tab').addClass("active");
          $('#boletin').removeClass("show active");
          $('#noticias').removeClass("show active");
          $('#eventos').addClass("show active");
          break;
        case 'noticias':
          $('#boletin-tab').removeClass("active");
          $('#eventos-tab').removeClass("active");
          $('#noticias-tab').addClass("active");
          $('#boletin').removeClass("show active");
          $('#eventos').removeClass("show active");
          $('#noticias').addClass("show active");
          break;
          case 'boletin':
          $('#boletin-tab').addClass("active");
          $('#eventos-tab').removeClass("active");
          $('#noticias-tab').removeClass("active");
          $('#boletin').addClass("show active");
          $('#noticias').removeClass("show active");
          $('#eventos').removeClass("show active");
            break;
        default:
          console.log('default case')
          break;
      }
    }
</script>
<script>

/**
*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://concimaz.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<script src="<?php echo $addRoute;?>js/commentsActions.js"></script>
<script src="<?php echo $addRoute;?>js/accountsActions.js"></script>
