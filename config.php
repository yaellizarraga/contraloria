<?php
if ($_SERVER['SERVER_NAME'] === 'concimaz.org' || $_SERVER['SERVER_NAME'] === 'www.concimaz.org' || $_SERVER['SERVER_NAME'] === 'www.concimaz.org/?toservices=true') {
    include_once 'config.production.php';
} else if ($_SERVER['SERVER_NAME'] === 'localhost' || strpos($_SERVER['SERVER_NAME'], '.ngrok.io') !== FALSE) {
    include_once 'config.development.php';
}
$_SESSION['main'] = TRUE;

// echo "Conected to: ".DB_HOST.' '.DB_USERNAME.''.DB_PASS.' '.DB_DB;
