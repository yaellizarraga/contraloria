<div id="loginModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login de acceso</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" id="emailLogin" aria-describedby="emailHelp" placeholder="Ingrese su email">
              <small id="emailHelp" class="form-text text-muted">Nunca compartimos su email con nadie</small>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" class="form-control" id="passLogin" placeholder="Ingrese la contraseña">
            </div>
            <div class="form-group form-check">
              <input type="checkbox" class="form-check-input" id="exampleCheck1">
              <label class="form-check-label" for="exampleCheck1">Recordarme</label>
            </div>
        </form>
        <div class="row">
          <div class="col-md-12" id="messageLogin"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="login()" class="btn btn-primary">Entrar</button>
      </div>
    </div>
  </div>
</div>
