<?php
session_start();
error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
unset($_SESSION['SERVICE']);
unset($_SESSION['TOPIC']);
unset($_SESSION['NEWS']);
unset($_SESSION['EVENTS']);
include_once('config.php');
require('conexion.php');

$conexion = new Conexion;
$conexion->conectar();
$eventos = $conexion->query("SELECT * FROM events ORDER BY date DESC;");
$noticias = $conexion->query("SELECT * FROM news ORDER BY date DESC;");
$niveles = $conexion->query("SELECT * FROM nivel_participaciones;");
$lastEvents = $conexion->query("SELECT * FROM events where idEvent=(select max(idEvent) from events);");
$lastEvent = mysqli_fetch_object($lastEvents);
$lastNews = $conexion->query("SELECT * FROM news where idNews=(select max(idNews) from news);");
$lastNew = mysqli_fetch_object($lastNews);
?>
<!DOCTYPE html>
<html lang="en">
  <?php require('head.php');?>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  <input type="hidden" id="bandera" value="<?php echo (isset($_GET['toservices']))?'1':'0'?>">
  <input type="hidden" id="bandera2" value="<?php echo (isset($_GET['toevents']))?'1':'0'?>">
  <input type="hidden" id="bandera3" value="<?php echo (isset($_GET['tonews']))?'1':'0'?>">
  <div class="site-wrap"  id="home-section">

    <?php require('menu.php');?>

    <div class="owl-carousel slide-one-item">
      <a href="#"><img src="images/slider/slide2.jpg" alt="Image" class="img-fluid"></a>
      <a href="<?php echo BASE_URL.'services/denuncias';?>"><img src="images/slider/slide3.jpg" alt="Image" class="img-fluid"></a>
      <a href="#" id="registro-slider"><img src="images/slider/slide1.jpg" alt="Image" class="img-fluid"></a>
    </div>
    <nav class="stickyMenu" id="floatMenu">
      <ul class="mainMenu">
        <li id="acercade"><a href="#">Acerca de</a>
          <ul class="subMenu">
            <li><a href="javascript:openAbout('mision')">Misión</a></li>
            <li><a href="javascript:openAbout('vision')">Visión</a></li>
            <li><a href="javascript:openAbout('valores')">Valores</a></li>
            <li><a href="javascript:openAbout('organigrama')">Directorio</a></li>
            <li><a href="javascript:openAbout('comisiones')">Comisiones</a></li>
          </ul>
        </li>
      </ul>
    </nav>
    <div class="site-section bg-image overlay counter" style="background-image: url('images/mazatlan.jpg');" id="about-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-lg-6 mb-4">
            <h2 class="text-primary-color mb-4 text-uppercase section-title">Quienes somos</h2>
            <p class="text-grey text-justify">La Contraloría Ciudadana de Mazatlán, A.C. se define como una Organización de la Sociedad Civil sin ánimo de lucro, ni de proselitismo partidista, político-electoral o religioso; la cual tiene como objeto social derivado del Artículo 5 de sus Estatuto: </p>
            <p class="text-grey text-justify">Promover el combate a la corrupción, la transparencia, la rendición de cuentas y el gobierno abierto, mediante la aplicación, eficaz, eficiente, honesta y económica del ingreso y gasto público del gobierno; en beneficio del bienestar social y el mejoramiento de la calidad de vida de la población, sobre todo en situación de pobreza, exclusión, desigualdad o vulnerabilidad social.</p>
            <figure class="block-img-video-1" data-aos="fade" id="videoMain">
              <iframe src="https://player.vimeo.com/video/382101874" width="100%" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
            </figure>
          </div>
          <div class="col-lg-5 ml-auto align-self-lg-center">
            <h2 class="text-primary-color mb-4 text-uppercase section-title">Objetivos</h2>
            <p class="text-grey text-justify"><strong>Para su cumplimiento promoverá entre otros, los siguientes objetivos específicos:</strong></p>
            <p class="text-grey text-justify">1. Promover el empoderamiento ciudadano en asuntos de interés público, mediante organización de foros, eventos académicos, asesoría, consulta, formación cívica y otros en temas de transparencia, rendición de cuentas, gobierno abierto y derecho de acceso a la información pública.</p>
            <p class="text-grey text-justify">2. Promover instancias de interlocución y colaboración entre gobierno, las organizaciones de la sociedad civil y el sector privado, para compartir puntos de vista en la ejecución del gasto público y programas de gobierno; impulsar procesos de organización comunitaria para la solución de problemas, de bienestar social y el mejoramiento de la calidad de vida de la población en situación de pobreza, exclusión, desigualdad o vulnerabilidad social.</p>
            <p class="text-grey text-justify">3. Promoción de la cultura de la legalidad y el combate a la corrupción y prevención de faltas administrativas graves y comisión de delitos en el quehacer público municipal, con acompañamiento ciudadano.</p>
            <p class="text-grey text-justify">4. Fomentar el rediseño de la administración pública municipal para mayor eficacia y eficiencia, su profesionalización, el uso de la Gestión por Resultados (GPR), el Presupuesto Basado en Resultados (PBR) y los sistemas de evaluación del desempeño, en base de indicadores de medición; y con el seguimiento, monitoreo y evaluación de las organizaciones de la sociedad civil, colegios de profesionistas y de instituciones educativas de nivel superior.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section" id="services-section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/informes";?>" class="services-link">
            <img src="images/icons/iconsPurple/iconoInformes.png" class="img-fluid" alt="servicio informes concimaz">
            <h3 class="text-primary-color h4 mb-2">Informes</h3>
            <p>Informes de actividades de gestión.</p>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/denuncias";?>" class="services-link">
            <img src="images/icons/iconsPurple/denuncia.png" class="img-fluid" alt="servicio denuncia concimaz">
            <h3 class="text-primary-color h4 mb-2">Denuncias ciudadanas</h3>
            <p>¿Observas algun acto de corrupción? Realiza tu denuncia aquí.</p>
          </a>
          </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/capacitacion";?>">
            <img src="images/icons/iconsPurple/capacitacion.png" class="img-fluid" alt="servicio capacitacion concimaz">
            <h3 class="text-primary-color h4 mb-2">Capacitación y Asesoría</h3>
            <p>Consulta los cursos y material disponible a traves de la red.</p>
          </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/licitacion";?>">
            <img src="images/icons/iconsPurple/licitaciones.png" class="img-fluid" alt="servicio licitaciones concimaz">
            <h3 class="text-primary-color h4 mb-2">Licitaciones públicas</h3>
            <p>Adquisiciones, arrendamientos y servicios del sector público.</p>
          </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/opiniones";?>">
            <img src="images/icons/iconsPurple/compilacion.png" class="img-fluid" alt="servicio compilacion concimaz">
            <h3 class="text-primary-color h4 mb-2">Compilación de opiniones en línea</h3>
            <p>Consulta opiniones registradas sobre temas y acontecimientos recientes en la vida pública de nuestro municipio.</p>
          </a>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 text-center">
            <a href="<?php echo BASE_URL."services/servidorespublicos";?>">
            <img src="images/icons/iconsPurple/registro.png" class="img-fluid" alt="servicio registro concimaz">
            <h3 class="text-primary-color h4 mb-2">Buró de Servidores públicos</h3>
            <p>Consulta y Califica el nivel de servicio ofrecido por el funcionario público de tu ciudad.</p>
          </a>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section" id="blog-section">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center mb-5">
            <h2 class="text-primary-color section-title text-uppercase">Información de interes</h2>
          </div>
        </div>
      <div class="row">
        <div class="col-lg-8">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" onclick="showTab('noticias')" id="noticias-tab" data-toggle="tab" href="#noticias" role="tab" aria-controls="home" aria-selected="true">Noticias</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="showTab('eventos')" id="eventos-tab" data-toggle="tab" href="#eventos" role="tab" aria-controls="profile" aria-selected="false">Eventos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" onclick="showTab('boletin')" id="boletin-tab" data-toggle="tab" href="#boletin" role="tab" aria-controls="profile" aria-selected="false">Boletin</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="noticias" role="tabpanel" aria-labelledby="noticias-tab">
              <div class="container-fluid">
                <?php
                while($noticia = mysqli_fetch_object($noticias)){
                  if($noticia->image != null){
                    $image = file_get_contents($noticia->image);
                    $content = base64_encode($image);
                    $data = "data:image/png;base64,{$content}";
                  }else{
                    $data = "noimage.jpg";
                  }
                ?>
                <div class="d-md-flex d-lg-flex flex-row justify-content-around mb-4 mt-4">
                  <div class="col-sm-12 col-md-4">
                    <div class="d-flex mb-5">
                      <img src="<?php echo $data;?>" class="img-fluid" alt="img">
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-8">
                    <div class="d-flex flex-column">
                      <a href="services/noticia/?news=<?php echo $noticia->idNews;?>"><h5><?php echo $noticia->title; ?></h5></a>
                      <p class="text-justify text-wrap" style="word-break: break-all;">
                        <?php echo $noticia->content;?>
                      </p>
                      <p style="color: #9e60b4;">
                          <strong><?php echo $noticia->date;?></strong>
                      </p>
                    </div>
                  </div>
                </div>
              <?php } ?>
              </div>
            </div>
            <div class="tab-pane fade" id="eventos" role="tabpanel" aria-labelledby="boletin-tab">
              <div class="container-fluid">
                <?php
                 while($evento = mysqli_fetch_object($eventos)){
                   if($evento->image != null){
                     $image = file_get_contents($evento->image);
                     $content = base64_encode($image);
                     $data = "data:image/png;base64,{$content}";
                   }else{
                     $data = "noimage.jpg";
                   }
                ?>
                <div class="d-md-flex d-lg-flex flex-row justify-content-around mb-4 mt-4">
                  <div class="col-sm-12 col-md-4">
                    <div class="d-flex mb-5">
                      <img src="<?php echo $data; ?>" class="img-fluid" alt="img">
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-8">
                    <div class="d-flex flex-column">
                      <a href="services/evento/?events=<?php echo $evento->idEvent;?>"><h5><?php echo $evento->title;?></h5></a>
                      <p class="text-justify text-wrap" style="word-break: break-all;">
                        <?php echo $evento->content;?>
                      </p>
                      <p style="color: #9e60b4;">
                        <strong><?php echo $evento->date;?></strong>
                      </p>
                    </div>
                  </div>
                </div>
              <?php } ?>
              </div>
            </div>
            <div class="tab-pane fade" id="boletin" role="tabpanel" aria-labelledby="boletin-tab">
              <div class="container-fluid">
                <div class="d-md-flex d-lg-flex flex-row justify-content-around mb-4 mt-4">
                  <div class="col-md-4">
                    <div class="d-flex mb-5">
                      <img src="noimage.jpg" class="img-fluid" alt="img">
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="d-flex flex-column">
                      <h5>Boletin test1</h5>
                      <p class="text-justify" style="word-break: break-all;">
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... <a href="#">Leer más</a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="d-md-flex d-lg-flex flex-row justify-content-around mb-4 mt-4">
                  <div class="col-md-4">
                    <div class="d-flex mb-5">
                      <img src="noimage.jpg" class="img-fluid" alt="img">
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="d-flex flex-column">
                      <h5>Boletin test2</h5>
                      <p class="text-justify text-wrap" style="word-break: break-all;">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... <a href="#">Leer más</a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="d-md-flex d-lg-flex flex-row justify-content-around mb-4 mt-4">
                  <div class="col-md-4">
                    <div class="d-flex mb-5">
                      <img src="noimage.jpg" class="img-fluid" alt="img">
                    </div>
                  </div>
                  <div class="col-md-8">
                    <div class="d-flex flex-column">
                      <h5>Boletin test3</h5>
                      <p class="text-justify" style="word-break: break-all;">
                       Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua... <a href="#">Leer más</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <h4 align="center">Lo ultimo</h3>
          <div class="newItem">
            <ul class="indicador">
              <li>BOLETÍN</li>
            </ul>
            <a href="#">
              <p>
                MEJORES PRACTICAS EN EL COMBATE A LA CORRUPCIÓN
              </p>
            </a>
            <p>
              11 octubre 2019
            </p>
          </div>
          <hr>
          <div class="newItem">
            <ul class="indicador">
              <li>NOTICIA</li>
            </ul>
            <a href="#">
              <p>
                <?php echo strtoupper($lastNew->title); ?>
              </p>
            </a>
              <p>
                <?php echo $lastNew->date;?>
              </p>
          </div>
          <hr>
          <div class="newItem">
            <ul class="indicador">
              <li>EVENTO</li>
            </ul>
            <a href="#">
              <p>
                <?php echo strtoupper($lastEvent->title);?>
              </p>
            </a>
            <p>
              <?php echo $lastEvent->date;?>
            </p>
          </div>
          <hr>
        </div>
      </div>
      </div>
    </div>

    <div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center mb-5">
            <h2 class="text-primary-color section-title text-uppercase">Contactanos</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <form id="contactForm">
              <div class="form-group row">
                <div class="col-md-6 mb-4 mb-lg-0">
                  <input type="text" class="form-control" id="txtnombres" name="nombres" placeholder="Nombres" autocomplete="false">
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="txtapellidos" name="apellidos" placeholder="Apellidos" autocomplete="false">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input type="email" name="email" class="form-control" id="txtemail" placeholder="Correo electronico" autocomplete="false">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="message" id="txtmessage" class="form-control" placeholder="Escribe tu mensaje." cols="30" rows="10" autocomplete="false"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 ml-auto">
                  <button class="btn btn-block btn-primary text-white py-3 px-5">Enviar mensaje</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-6">
            <div class="row" style="margin-bottom: 1%;">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3666.970251952271!2d-106.4219973857139!3d23.207754715206743!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869f5397819f721d%3A0xc6ec8873eba05371!2sDr.%20Carvajal%202608%2C%20Centro%2C%2082000%20Mazatl%C3%A1n%2C%20Sin.!5e0!3m2!1ses!2smx!4v1577132182529!5m2!1ses!2smx" width="100%" height="300px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>

              <p>
                <span class="icon-office"></span> DR. CARVAJAL 2608 - 2 Col. Centro. C.P. 82000 Mazatlan, Sinaloa.
              </p>
              <p>
                <span class="icon-phone"></span> 669 1934375
              </p>
              <p>
                <span class="icon-mobile"></span> 669 116 0319
              </p>

          </div>
        </div>
      </div>
    </div>
    <a id="fixed-button" class="btn btn-large" type="button" href="javascript:toTop()"><span class="icon-arrow-up"></span></a>
    <?php require('footer.php');?>
  </div>
  <?php require('loginModal.php'); ?>
  <?php require('registroModal.php'); ?>
  <?php require('modalAbout.php'); ?>
  <?php require('alertModal.php'); ?>
  <?php require('scripts.php');?>
  </body>
</html>
