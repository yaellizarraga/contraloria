function getInsertedComment(post){
  $.ajax({
    method: 'POST',
    url: 'services/opiniones/actions.php',
    data: {
      "getComment": true,
      "idComment": post
    },
    success: function(res){
      return res;
    },
    dataType: 'json'
  });
}

function sendComment(username, postId) {
  $.ajax({
    method: 'POST',
    url:'services/opiniones/actions.php',
    data: {
      comment: true,
      idPost: postId,
      commentText: $('#txtComment').val()
    },
    success: function(result) {
      if(result){
        var post = getInsertedComment(result.idComment);
        console.log(post);
        var coomment = `<div class="card comment${post.id}">
      	    <div class="card-body">
        	        <div class="row">
              	    <div class="col-md-2">
              	        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
              	        <p class="text-secondary text-center">15 Minutes Ago</p>
              	    </div>
              	    <div class="col-md-10">
              	       <p>
              	         <a class="float-left" href="#"><strong>${username}</strong></a>
              	       </p>
              	       <div class="clearfix"></div>
              	        <p>${$('#txtComment').val()}</p>
              	        <p>
              	            <button class="float-right btn text-white btn-primary ml-2"> <i class="fa fa-reply"></i> Reply</button>
              	            <button class="float-right btn text-white btn-primary ml-2"> <i class="fa fa-hand-o-down"></i> Dislike</button>
                            <button class="float-right btn text-white btn-primary"> <i class="fa fa-heart"></i> Like</button>
              	       </p>
              	    </div>
      	        </div>
                <div class="row" class="commentBox${post.id}">
                  <div class="col-md-8 offset-2">
                        <input type="text" class="form-control">
                        <button type="button" class="flot-rigth btn text-white btn-primary mt-2" name="button" onclick="sendReply()">Comentar</button>
                  </div>
                </div>
      	        	<div class="card card-inner replies">
      	         </div>
      	    </div>
      	</div>`;
        $('#comments').append(comment);
      }else{
        alert('Encontramos un error al enviar su comentario, intente mas tarde.');
      }
    }
  });
}

function showReplyBox(comment) {
  $('').show();
}

function sendReply() {
  $.ajax({
    method: 'POST',
    url:'services/opiniones/actions.php',
    data: {
      reply: true,
      idUser: $('#hiddenIdUser').val(),
      replyComment: $('').val(),
    },
    success: function(result) {

      var reply = `<div class="card-body">
          <div class="row">
              <div class="col-md-2">
                  <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                  <p class="text-secondary text-center">15 Minutes Ago</p>
              </div>
              <div class="col-md-10">
                  <p><a href="#"><strong>Jorge Alberto Herrera Gonzalez</strong></a></p>
                  <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                  <p>
                      <button class="float-right btn text-white btn-primary ml-2"> <i class="fa fa-hand-o-down"></i> Dislike</button>
                      <button class="float-right btn text-white btn-primary"> <i class="fa fa-heart"></i> Like</button>
                 </p>
              </div>
          </div>
      </div>`;

    }
  });
}

function sendLike() {
  $.ajax({
    method: 'POST',
    url:'services/opiniones/actions.php',
    data: {
      "like": true
    },
    success: function(result) {

    }
  });
}

function sendDislike() {
  $.ajax({
    method: 'POST',
    url:'services/opiniones/actions.php',
    data: {
      "dislike": true
    },
    success: function(result) {

    }
  });
}
