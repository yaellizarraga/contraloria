function returnVacio(div){
  var alert = `<div class="alert alert-${'danger'} alert-dismissible fade show" role="alert">
                ${'Debe capturar todos los campos.'}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>`;
              $(div).html("");
              $(div).append(alert);
}
function returnMessage(message) {
  var alert = `<div class="alert alert-${'danger'} alert-dismissible fade show" role="alert">
                ${message}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>`;
              $('#messageRegister').html("");
              $('#messageRegister').append(alert);
}
function enviaBienvenida(user, email) {
    $.ajax({
      method: 'POST',
      url: 'lib/sendWelcome.php',
      data:{
        welcome: true,
        user: user,
        email: email
      },
      dataType: 'json'
    });
}

function insertAccount() {
  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  if($('#nivelParti').val() === 0) { returnVacio('#messageRegister'); return false; }
  if($('#nombre').val() === "") { returnVacio('#messageRegister'); return false; }
  if($('#apellido').val() === "") { returnVacio('#messageRegister'); return false; }
  if($('#telefono').val() === "") { returnVacio('#messageRegister'); return false; }
  if($('#emailRegistro').val() === "") { returnVacio('#messageRegister'); return false; }
  if($('#con_pass').val() === "") { returnVacio('#messageRegister'); return false; }
  if($('#passRegistro').val() === "") { returnVacio('#messageRegister'); return false; }
  if(!emailReg.test($('#emailRegistro').val())) { returnMessage('Capture un email valido.'); return false; }
  if($('#emailRegistro').val() !== $('#con_email').val()){ returnMessage('Los correos no coinciden.'); return false; }
  if($('#passRegistro').val() !== $('#con_pass').val()){ returnMessage('Las contraseñas no coinciden'); return false;}
  $.ajax({
    method:'post',
    url:'services/accountsActions.php',
    data:{
      newAccount: true,
      idNiv: $('#nivelParti').val(),
      nombre: $('#nombre').val(),
      apellido: $('#apellido').val(),
      telefono: $('#telefono').val(),
      email: $('#emailRegistro').val(),
      password: $('#passRegistro').val()
    },
    success: function(result) {
      console.log(result);
      if(result.message !== undefined && result.message ==="exist"){
        var alert = `<div class="alert alert-${'danger'} alert-dismissible fade show" role="alert">
                      ${'Ya existe un usuario con el email proporcionado.'}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>`;
                    $('#messageRegister').html("");
                    $('#messageRegister').append(alert);
                    $('#emailRegistro').val("");
                    $('#con_pass').val("");
                    $('#con_email').val("");
                    $('#emailRegistro').focus();
                    return false;
      }
      if(result){
        var alert = `<div class="alert alert-${'success'} alert-dismissible fade show" role="alert">
                      ${'Cuenta creada con exito!!'}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>`;
                    enviaBienvenida($('#nombre').val()+' '+$('#apellido').val(), $('#emailRegistro').val());
                    $('#nivelParti').val(0);
                    $('#nombre').val("");
                    $('#apellido').val("");
                    $('#telefono').val("");
                    $('#emailRegistro').val("");
                    $('#con_pass').val("");
                    $('#con_email').val("");
                    $('#passRegistro').val("");
                    $('#registroModal').modal('hide');
      }else{
        var alert = `<div class="alert alert-${'danger'} alert-dismissible fade show" role="alert">
                      ${'Encontramos un problema al crear su cuenta, intente mas tarde.'}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>`;
                    $('#nivelParti').val(0);
                    $('#nombre').val("");
                    $('#apellido').val("");
                    $('#telefono').val("");
                    $('#emailRegistro').val("");
                    $('#con_pass').val();
                    $('#passRegistro').val("");
      }
      $('#messageRegister').html("");
      $('#messageRegister').append(alert);
    },
    dataType: 'JSON'
  });
}

function login() {
  if($('#emailLogin').val() === "") { returnVacio('#messageLogin'); return false; }
  if($('#passLogin').val() === "") { returnVacio('#messageLogin'); return false; }
  if(!$('#terminos').prop("checked")) {
    var alert = `<div class="alert alert-${'warning'} alert-dismissible fade show" role="alert">
                  ${'Debe aceptar los terminos antes de continuar.'}
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>`;
                $('#messageLogin').html("");
                $('#messageLogin').append(alert);
  }
  $.ajax({
    method:'POST',
    url:'services/accountsActions.php',
    data:{
      "login": true,
      "email": $('#emailLogin').val(),
      "pass": $('#passLogin').val()
    },
    success: function(result) {
      if(result.account !== false){
        $('#UserNameContainer').html("");
        $('#UserNameContainer').append(`<a href="#"><strong>Ciudadano: ${result.account.nombre} ${result.account.apellido}</strong></a><div class="dividerTop"></div><a href="logout.php">Salir</a>`);
        $('#loginModal').modal('hide');
        $('#emailLogin').val("");
        $('#passLogin').val("");
      }else{
        var alert = `<div class="alert alert-${'warning'} alert-dismissible fade show" role="alert">
                      ${'Usuario o contraseña incorrectos'}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>`;
        $('#messageLogin').append(alert);
      }
    },
    dataType: 'json'
  });
}
