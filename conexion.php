<?php

class Conexion {
    private $host = DB_HOST;
    private $user = DB_USERNAME;
    private $password = DB_PASS;
    private $database = DB_DB;
    public $con;
    public $result;

    public function conectar(){
      try{
        $this->con = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        return $this->con;
      }catch(mysqli_sql_exception  $e){
        throw $e;
      }
    }

    public function query($sql){
      mysqli_query($this->con, "USE ".$this->database.";");
      if($this->result = mysqli_query($this->con, $sql)){
        return $this->result;
      }else{
        return mysqli_error($this->con);
      }
    }

    public function desconectar($conexion){
      try{
        mysqli_close($conexion);
      }catch(mysqli_sql_exception $e){
        throw $e;
      }
    }
}
