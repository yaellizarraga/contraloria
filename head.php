<?php
if(isset($_SESSION['SERVICE']) || isset($_SESSION['NEWS']) || isset($_SESSION['EVENTS'])) {
  $addRoute = "../../";
} else if(isset($_SESSION['TOPIC'])) {
  $addRoute = "../../../";
} else {
  $addRoute = "";
}
?>
<head>
  <title><?php echo (isset($title))?$title:'CONCIMAZ | CONTRALORIA CIUDADANA DE MAZATLAN'; ?></title>
  <?php
    if(isset($page) && $page=="licitaciones"){
  ?>
  <meta name="title" content="CONCIMAZ | CONTRALORIA CIUDADANA DE MAZATLAN | Licitaciones ">
  <meta property="og:title" content="CONCIMAZ | CONTRALORIA CIUDADANA DE MAZATLAN | Licitaciones "/>
  <!-- darle prioridad a este que es el que se llena en cq-->
  <meta name="description" content="La CONCIMAZ (CONTRALORIA CIUDADANA DE MAZATLAN) te informa de las licitaciones Publicas en el Municipio de Mazatlán como son adquisiciones, servicios, arrendamientos y enajenaciones, un procedimiento administrativo de preparación de la voluntad contractual, por el que un ente público en ejercicio de la función administrativa invita a los interesados para que, sujetándose a las bases fijadas en el pliego de condiciones, formulen propuestas."/>
  <meta property="pageDescription" content="La CONCIMAZ (CONTRALORIA CIUDADANA DE MAZATLAN) te informa de las licitaciones Publicas en el Municipio de Mazatlán como son adquisiciones, servicios, arrendamientos y enajenaciones, un procedimiento administrativo de preparación de la voluntad contractual, por el que un ente público en ejercicio de la función administrativa invita a los interesados para que, sujetándose a las bases fijadas en el pliego de condiciones, formulen propuestas."/>
  <meta property="og:pageDescription" content="La CONCIMAZ (CONTRALORIA CIUDADANA DE MAZATLAN) te informa de las licitaciones Publicas en el Municipio de Mazatlán como son adquisiciones, servicios, arrendamientos y enajenaciones, un procedimiento administrativo de preparación de la voluntad contractual, por el que un ente público en ejercicio de la función administrativa invita a los interesados para que, sujetándose a las bases fijadas en el pliego de condiciones, formulen propuestas."/>
  <?php } ?>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="cache-control" content="no-cache"/>
  <meta property="og:description" content="La Contraloría Ciudadana de Mazatlán, A.C. se define como una Organización de la Sociedad Civil sin ánimo de lucro, ni de proselitismo partidista, político-electoral o religioso; la cual tiene como objeto promover el combate a la corrupción, la transparencia, la rendición de cuentas y el gobierno abierto, mediante la aplicación eficaz, eficiente y honesta del ingreso y gasto público del gobierno del Municipio de Mazatlán."/>
  <meta name="description" content="La Contraloría Ciudadana de Mazatlán, A.C. se define como una Organización de la Sociedad Civil sin ánimo de lucro, ni de proselitismo partidista, político-electoral o religioso; la cual tiene como objeto promover el combate a la corrupción, la transparencia, la rendición de cuentas y el gobierno abierto, mediante la aplicación eficaz, eficiente y honesta del ingreso y gasto público del gobierno del Municipio de Mazatlán."/>
  <meta name="keywords" content="transparencia, participación, colaboración, rendición-de-cuentas, gobierno-abierto, corrupción">
  <meta name="tags" content="transparencia, participación, colaboración, rendición-de-cuentas, gobierno-abierto, corrupción">
  <meta name="author" content="Tecnología Informática Móvil">
  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700|Anton" rel="stylesheet">

  <link rel="icon" href="<?php echo $addRoute;?>favicon.ico">
  <link rel="stylesheet" href="<?php echo $addRoute;?>fonts/icomoon/style.css">

  <link rel="stylesheet" href="<?php echo $addRoute;?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $addRoute;?>css/magnific-popup.css">
  <link rel="stylesheet" href="<?php echo $addRoute;?>css/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo $addRoute;?>css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo $addRoute;?>css/owl.theme.default.min.css">

  <link rel="stylesheet" href="<?php echo $addRoute;?>css/bootstrap-datepicker.css">

  <link rel="stylesheet" href="<?php echo $addRoute;?>fonts/flaticon/font/flaticon.css">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

  <link rel="stylesheet" href="<?php echo $addRoute;?>css/aos.css">

  <link rel="stylesheet" href="<?php echo $addRoute;?>css/style.css">
</head>
