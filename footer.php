<?php
if(isset($_SESSION['SERVICE'])) {
  $addRoute = "../../";
} else if(isset($_SESSION['TOPIC'])) {
  $addRoute = "../../../";
} else {
  $addRoute = "";
}
?>
<footer class="site-footer bg-image-grey overlay counter" style="background-image: url('<?php echo $addRoute;?>images/mazatlan.jpg');" id="footer-site">
  <div class="container">
    <div class="row mt-5">
      <div class="col-md-8">
        <div class="row">
          <div class="col-md-8">
            <h2 class="footer-heading mb-4">Acerca de nosotros</h2>
            <p class="text-justify">La Contraloría Ciudadana de Mazatlán, A.C. se define como una Organización de la Sociedad Civil sin ánimo de lucro, ni de proselitismo partidista, político-electoral o religioso.</p>
            <p class="text-justify">Promover el combate a la corrupción, la transparencia, la rendición de cuentas y el gobierno abierto, mediante la aplicación, eficaz, eficiente, honesta y económica del ingreso y gasto público del gobierno; en beneficio del bienestar social y el mejoramiento de la calidad de vida de la población, sobre todo en situación de pobreza, exclusión, desigualdad o vulnerabilidad social</p>
          </div>
          <div class="col-md-4 ml-auto">
            <h2 class="footer-heading mb-4">Enlaces de interes</h2>
            <ul class="list-unstyled">
              <li><a href="http://www.ceaipsinaloa.org.mx/" target="_blank"><img src="<?php echo BASE_URL.'images/ceaip.png'?>" class="img-link" alt=""></a></li>
              <li><a href="http://inicio.inai.org.mx/SitePages/ifai.aspx" target="_blank"><img src="<?php echo BASE_URL.'images/inai.png'?>" class="img-link" alt=""></a></li>
              <li><a href="http://www.infomexsinaloa.org.mx/infomexsinaloa/Default.aspx" target="_blank"><img src="<?php echo BASE_URL.'images/infomex.png'?>" class="img-link" alt=""></a></li>
              <li><a href="https://www.plataformadetransparencia.org.mx/web/guest/inicio" target="_blank"><img src="<?php echo BASE_URL.'images/pnt.png'?>" class="img-link" alt=""></a></li>
            </ul>
          </div>

        </div>
      </div>
      <div class="col-md-4 ml-auto">
        <div class="mb-5">
          <div class="mb-5">
            <h2 class="footer-heading mb-4">Atención Ciudadana</h2>
            <p class="text-justify">Las dudas, sugerencias y comentarios que quiera usted hacer sobre la actuación de la CONCIMAZ, lo atenderemos <a href="#contact-section">aquí</a> para su atención y seguimiento.</p>
          </div>
          <h2 class="footer-heading mb-4">Suscribete a nuestro boletín</h2>
          <form action="#" method="post">
            <div class="input-group mb-3">
              <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
              <div class="input-group-append">
                <button class="btn btn-primary text-white" type="button" id="button-addon2">Suscribir</button>
              </div>
            </div>
          </div>
          <h2 class="footer-heading mb-4">Siguenos</h2>
            <a href="https://www.facebook.com/contraloriaciudadanamzt/" target="_blank" class="footer-social-icon pl-0 pr-3"><span class="icon-facebook"></span></a>
            <a href="https://twitter.com/ContraloraCiud2" target="_blank" class="footer-social-icon pl-3 pr-3"><span class="icon-twitter"></span></a>
            <a href="https://www.instagram.com/concimaz/" target="_blank" class="footer-social-icon pl-3 pr-3"><span class="icon-instagram"></span></a>
            <a href="https://www.youtube.com/channel/UCjTuIaNvzpYsSS19TLRTQpg" target="_blank" class="footer-social-icon pl-3 pr-3"><span class="icon-youtube"></span></a>
        </form>
      </div>
    </div>
    <div class="row pt-1 mt-1 text-center">
      <div class="col-md-12">
        <div class="border-top pt-5">
        <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Desarrollado by Tecnología Informática Móvil | <a target="_blank" href="pdf/privacidad.pdf">Terminos y Condiciones</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        </p>
        </div>
      </div>
    </div>
  </div>
</footer>
