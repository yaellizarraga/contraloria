<?php
  session_start();
  $_SESSION['SERVICE'] = TRUE;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../head.php');?>
  <body>
    <div class="site-wrap">
      <?php require('../../menu.php');?>
      <div class="site-section bg-light counter" id="discover-section">
        <div class="container">
          <div class="flex-column mb-3" id="denuncia-logo">
            <div class="d-flex justify-content-center">
                <img src="img/informes.png" class="img-fluid" alt="Denuncia service">
            </div>
            <div class="d-flex justify-content-center">
                <h3>Informes</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="owl-carousel slide-one-item">
                <a href="#"><img src="../../images/slider/slide2.jpg" alt="Image" class="img-fluid"></a>
                <a href="#"><img src="../../images/slider/slide1.jpg" alt="Image" class="img-fluid"></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php require('../../footer.php');?>
    </div>
    <?php require('../../loginModal.php'); ?>
    <?php require('../../registroModal.php'); ?>
    <?php require('../../modalAbout.php'); ?>
    <?php require('../../scripts.php');?>
  </body>
</html>
