<?php
error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
session_start();
unset($_SESSION['TOPIC']);
$_SESSION['SERVICE'] = TRUE;
require('../../config.php');
include('../../conexion.php');
$bd = new Conexion;
$bd->conectar();
$niveles = $bd->query("SELECT * FROM nivel_participaciones;");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../head.php');?>
  <body>
    <div class="site-wrap">
    <?php require('../../menu.php');?>
    <div class="site-section bg-light counter" id="discover-section">
      <div class="container">
        <div class="flex-column" id="denuncia-logo">
          <div class="d-flex justify-content-center">
              <img src="img/denuncia.png" class="img-fluid" alt="Denuncia service">
          </div>
          <div class="d-flex justify-content-center">
              <h3>Denuncia Ciudadana</h3>
          </div>
        </div>
        <div class="row mt-5 mb-5 justify-content-center" id="denuncia-form">
          <div class="col-lg-6">
            <form id="denunciaForm">
              <div class="form-group text-right">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" value="" id="checkDenuncia">
                  <label class="form-check-label" for="defaultCheck1">
                    Denuncia Anonima
                  </label>
                </div>
              </div>
              <div class="form-group row personal-data">
                <div class="col-lg-6">
                    <h4>Datos del denunciante</h4>
                </div>
              </div>
              <div class="form-group row personal-data">
                <div class="col-md-12 mb-4 mb-lg-0">
                  <input type="text" name="txtden_nombre" id="txtden_nombre" class="form-control" placeholder="Nombre(s), Apellido(s)" autocomplete="off">
                </div>
              </div>
              <div class="form-group row personal-data">
                <div class="col-md-12">
                  <input type="email" name="txtden_email" id="txtden_email" class="form-control" placeholder="Correo Electrónico" autocomplete="off">
                </div>
              </div>
              <div class="form-group row personal-data">
                <div class="col-md-12">
                  <input type="number" name="txtden_telefono" id="txtden_telefono" class="form-control" placeholder="Número Telefónico" autocomplete="off">
                </div>
              </div>
              <div class="form-group row personal-data">
                <div class="col-md-12">
                  <input type="text" name="txtden_domicilio" id="txtden_domicilio" class="form-control" placeholder="Domicilio" autocomplete="off">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                    <h4>Datos del denunciado</h4>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12 mb-4 mb-lg-0">
                  <input type="text" name="txtden_denunciado" id="txtden_denunciado" class="form-control" placeholder="Nombre(s), Apellido(s)" autocomplete="off">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <select class="form-control" name="entePub" id="cmbden_ente">
                      <option value="0">Seleccione ente público</option>
                      <optgroup label="H. Ayuntamiento de Mazatlán">
                        <option value="Ayuntamiento">Coordinación De Acceso A La Información</option>
                        <option value="Ayuntamiento">Centro De Atención Y Protección Al Turista (Capta)</option>
                        <option value="Ayuntamiento">Coordinación De Protección Civil Municipal</option>
                        <option value="Ayuntamiento">Coordinación De Tribunal De Barandilla</option>
                        <option value="Ayuntamiento">Dirección De Atención Ciudadana</option>
                        <option value="Ayuntamiento">Dirección De Bienestar Y Desarrollo Social</option>
                        <option value="Ayuntamiento">Dirección Ecología Y Medio Ambiente</option>
                        <option value="Ayuntamiento">Dirección De Egresos</option>
                        <option value="Ayuntamiento">Dirección De Ev. Y Enlace Zona Rural</option>
                        <option value="Ayuntamiento">Dirección De Informática</option>
                        <option value="Ayuntamiento">Dirección De Ingresos</option>
                        <option value="Ayuntamiento">Dirección De Obras Públicas</option>
                        <option value="Ayuntamiento">Dirección De Planeación Del Desarrollo Urbano Sustentable</option>
                        <option value="Ayuntamiento">Dirección De Relaciones Públicas</option>
                        <option value="Ayuntamiento">Dirección De Servicios Médicos Municipales</option>
                        <option value="Ayuntamiento">Dirección De Servicios Públicos</option>
                        <option value="Ayuntamiento">Dirección De Vivienda Y Tenencia De La Tierra</option>
                        <option value="Ayuntamiento">Dirección General De Comunicación Social</option>
                        <option value="Ayuntamiento">Oficialía Mayor</option>
                        <option value="Ayuntamiento">Órgano Interno De Control</option>
                        <option value="Ayuntamiento">Presidencia</option>
                        <option value="Ayuntamiento">Secretaría De Desarrollo Económico</option>
                        <option value="Ayuntamiento">Secretaria De La Presidencia</option>
                        <option value="Ayuntamiento">Secretaría De Seguridad Pública</option>
                        <option value="Ayuntamiento">Secretaría Del Ayuntamiento</option>
                        <option value="Ayuntamiento">Síndico Procurador</option>
                        <option value="Ayuntamiento">Tesorería</option>
                        <option value="Ayuntamiento">Regidor</option>
                      </optgroup>
                      <optgroup label="Jumapam">
                        <option value="Jumapam">Gerencia Comercial</option>
                        <option value="Jumapam">Gerencia de Administración y Finanzas</option>
                        <option value="Jumapam">Gerencia de Construcción</option>
                        <option value="Jumapam">Gerencia de Distribución</option>
                        <option value="Jumapam">Gerencia de Órgano Interno de Control</option>
                        <option value="Jumapam">Gerencia de Planeación Física</option>
                        <option value="Jumapam">Gerencia de Producción</option>
                        <option value="Jumapam">Gerencia General</option>
                      </optgroup>
                      <optgroup label="Acuario">
                        <option value="Acuario">Dirección Acuario Mazatlán</option>
                        <option value="Acuario">Administración Acuario Mazatlán</option>
                        <option value="Acuario">Contabilidad Acuario Mazatlán</option>
                        <option value="Acuario">Mercadotecnia y Comunicación</option>
                        <option value="Acuario">Contraloría Interna</option>
                        <option value="Acuario">Recursos Humanos</option>
                        <option value="Acuario">Educación Ecológica</option>
                        <option value="Acuario">Área Técnica</option>
                        <option value="Acuario">Información Acuario Mazatlán</option>
                      </optgroup>
                      <optgroup label="Immujer">
                        <option value="Immujer">Directora IMMUJER</option>
                        <option value="Immujer">Subdirectora Administrativa de IMMUJER</option>
                        <option value="Immujer">Órgano Interno de Control</option>
                        <option value="Immujer">Coordinadora del Departamento de Contabilidad</option>
                        <option value="Immujer">Coordinadora del Departamento de Trabajo Social</option>
                        <option value="Immujer">Coordinadora del Departamento Jurídico</option>
                        <option value="Immujer">Coordinadora del Departamento de Psicología</option>
                        <option value="Immujer">Coordinadora de Transversalización y Perspectiva de Genero</option>
                        <option value="Immujer">Coordinación de Enlace</option>
                        <option value="Immujer">Auxiliar de Psicología</option>
                        <option value="Immujer">Recepcionista</option>
                        <option value="Immujer">Asistente de Dirección y Recepción</option>
                      </optgroup>
                      <optgroup label="Instituto Municipal de la Juventud">
                        <option value="IMJU">Directora del Instituto Municipal de la Juventud</option>
                        <option value="IMJU">Subdirección de Administración y Finanzas</option>
                        <option value="IMJU">Asistente de Dirección</option>
                        <option value="IMJU">Coordinación de Becas</option>
                        <option value="IMJU">Coordinador de Programas y Proyectos</option>
                        <option value="IMJU">Coordinación Operativa y Logística</option>
                        <option value="IMJU">Coordinación de Vinculación Académica</option>
                        <option value="IMJU">Coordinación de Asuntos Jurídicos</option>
                        <option value="IMJU">Coordinación de Colonias y Zona Rural</option>
                        <option value="IMJU">Contralor Interno</option>
                      </optgroup>
                      <optgroup label="IMPLAN">
                        <option value="IMPLAN">Dirección General Implan</option>
                        <option value="IMPLAN">Control Interno</option>
                        <option value="IMPLAN">Asistente</option>
                        <option value="IMPLAN">Subdirección Técnica</option>
                        <option value="IMPLAN">Subdirección En Planeación Urbana Y Medio Ambiente</option>
                        <option value="IMPLAN">Unidad Administrativa</option>
                        <option value="IMPLAN">Departamento De Medio Ambiente Y Recursos Naturales</option>
                        <option value="IMPLAN">Unidad De Información Y Vinculación Departamento De Planeación Urbana RegionalSocial</option>
                        <option value="IMPLAN">Departamento De Sistemas De Información</option>
                        <option value="IMPLAN">Departamento De Diseño Y Proyectos</option>
                        <option value="IMPLAN">Departamento De Planeación Urbana Regional</option>
                      </optgroup>
                      <optgroup label="IMDEM">
                        <option value="IMDEM">Director del IMDEM</option>
                        <option value="IMDEM">Subdirector</option>
                        <option value="IMDEM">Órgano Interno de Control</option>
                        <option value="IMDEM">Administrador</option>
                        <option value="IMDEM">Coordinador del deporte federado y adaptado</option>
                        <option value="IMDEM">Coordinadora de ligas y clubes</option>
                        <option value="IMDEM">Coordinador de Mercadotecnia, publicidad y promoción</option>
                        <option value="IMDEM">Coordinador de asuntos jurídicos</option>
                        <option value="IMDEM">Coordinador de promoción deportiva</option>
                        <option value="IMDEM">Coordinador de infraestructura deportiva</option>
                        <option value="IMDEM">Coordinador de comunicación social</option>
                        <option value="IMDEM">Contador</option>
                        <option value="IMDEM">Jefe de ingresos</option>
                        <option value="IMDEM">Jefa de recursos humanos</option>
                        <option value="IMDEM">Responsable de la unidad de transparencia y unidad de archivo</option>
                      </optgroup>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <input type="text" class="form-control" name="txtden_puesto" id="txtden_puesto" placeholder="Puesto que desempeña" autocomplete="off">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6">
                  <label for="">Fecha de los hechos</label>
                  <input type="date" name="dtphechos" id="dtpden_hechos" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="txtden_descripcion" id="txtden_descripcion" class="form-control" placeholder="Descripción de los hechos." cols="30" rows="10" autocomplete="off"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 ml-auto">
                  <button class="btn btn-block btn-primary text-white py-3 px-5">Enviar denuncia</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
    <?php require('../../footer.php');?>
  </div>
  <?php require('../../alertModal.php'); ?>
  <?php require('../../loginModal.php'); ?>
  <?php require('../../registroModal.php'); ?>
  <?php require('../../modalAbout.php'); ?>
  <?php require('../../scripts.php');?>
  </body>
</html>
