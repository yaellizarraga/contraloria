<?php
session_start();
$_SESSION['SERVICE'] = TRUE;
$title = 'CONCIMAZ | CONTRALORIA CIUDADANA DE MAZATLAN | Licitaciones';
$page = "licitaciones";
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../head.php');?>
  <body>
    <div class="site-wrap">
    <?php require('../../menu.php');?>
    <div class="site-section bg-light counter" id="discover-section">
      <div class="container">
        <div class="flex-column" id="denuncia-logo">
          <div class="d-flex justify-content-center">
              <img src="img/licitacion.png" class="img-fluid" alt="Denuncia service">
          </div>
          <div class="d-flex justify-content-center">
              <h3>Licitaciones publicas</h3>
          </div>
        </div>
        <div class="row mb-5 mt-5 justify-content-center">
          <div class="col-sm-12">
            <p class="text-justify">
               Para garantizar un ahorro en el tiempo de búsqueda y abarcar todas las fuentes de consulta posible, ofrecemos el servicio de notificación y alerta de consursos y convocatorias, analizados y filtrados por la descripción precisa y los requerimientos específicos sobre el producto/servicio solicitado.
            </p>
            <p class="text-justify">
               Nuestra plataforma online funciona las 24 hrs, los 365 días del año, manteniendo un sistema de notificaciones a través de correo electrónico pudiendo acceder en cualquier dispositivo que cuente con conexión a internet.
            </p>
          </div>
        </div>
        <div class="flex-column mb-5">
          <div class="d-flex justify-content-center">
              <h3>Suscribase</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <form action="#" method="post">
              <div class="form-group row">
                <div class="col-md-12 mb-4 mb-lg-0">
                  <input type="text" class="form-control" placeholder="Nombres(s), Apellido(s)">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <input type="email" class="form-control" placeholder="Correo electronico">
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <select class="form-control" name="licitacionService">
                      <option value="0">¿En que servicio esta interesado?</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="" id="" class="form-control" placeholder="Escribe tu mensaje." cols="30" rows="10"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 ml-auto">
                  <button class="btn btn-block btn-primary text-white py-3 px-5">Enviar mensaje</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="d-flex flex-row">
              <div>
              </div>
              <div>
                Horario de servicio<br>
                8:00 am a 3:00 pm
              </div>
            </div>
            <div class="d-flex flex-row">
              <div>
              </div>
              <div>
                6691160319
              </div>
            </div>
            <div class="d-flex flex-row">
              <div>
              </div>
              <div>
                contraloriaciudadanamzt@gmail.com
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require('../../footer.php');?>
  </div>
  <?php require('../../loginModal.php'); ?>
  <?php require('../../registroModal.php'); ?>
  <?php require('../../modalAbout.php'); ?>
  <?php require('../../scripts.php');?>
  </body>
</html>
