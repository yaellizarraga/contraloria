<?php
  session_start();
  $_SESSION['SERVICE'] = TRUE;
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../head.php');?>
  <body>
    <div class="site-wrap">
    <?php require('../../menu.php');?>
    <div class="site-section bg-light counter" id="discover-section">
      <div class="container">
        <div class="flex-column mb-3" id="denuncia-logo">
          <div class="d-flex justify-content-center">
              <img src="img/capacitacion.png" class="img-fluid" alt="Denuncia service">
          </div>
          <div class="d-flex justify-content-center">
              <h3>Capacitaciones</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-6 col-lg-6">
            <div class="owl-carousel slide-one-item">
              <a href="#"><img src="../../images/slider/slide2.jpg" alt="Image" class="img-fluid"></a>
              <a href="#"><img src="../../images/slider/slide1.jpg" alt="Image" class="img-fluid"></a>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 col-lg-6">
            <p class="text-justify">
              Información de los beneficios que ofrecen los cursos. La capacitación en línea es un curso enfocado a emprendedores y empresarios a saber cómo encontrar y analizar las ideas de negocio exitosas
              </p>
              <p class="text-justify">
                En capacitación personal se creó un curso llamado IDEAS el cual esta enfocado a esta búsqueda de ideas y oportunidades exitosas para hacer negocios en internet. Esta capacitación aprenderas a realizar un análisis sistematico que te permitirá seleccinar la idea con mayor probabilidad de exito.
              </p>
          </div>
        </div>
      </div>
    </div>
    <?php require('../../footer.php');?>
  </div>
  <?php require('../../loginModal.php'); ?>
  <?php require('../../registroModal.php'); ?>
  <?php require('../../modalAbout.php'); ?>
  <?php require('../../scripts.php');?>
  </body>
</html>
