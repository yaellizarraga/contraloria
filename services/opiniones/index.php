<?php
  error_reporting(-1); // reports all errors
  ini_set("display_errors", "1"); // shows all errors
  ini_set("log_errors", 1);
  session_start();
  unset($_SESSION['TOPIC']);
  $_SESSION['SERVICE'] = TRUE;
  require('../../config.php');
  include('../../conexion.php');
  $bd = new Conexion;
  $bd->conectar();
  $posts = $bd->query("SELECT * FROM posts WHERE status = 1;");
  $niveles = $bd->query("SELECT * FROM nivel_participaciones;");
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../head.php');?>
  <body>
    <?php require('../../menu.php');?>
    <div class="container">
      <div class="flex-column" id="denuncia-logo">
        <div class="d-flex justify-content-center">
            <img src="img/compilacion.png" class="img-fluid" alt="Denuncia service">
        </div>
        <div class="d-flex justify-content-center">
            <h3>Recopilación de opiniones en línea</h3>
        </div>
      </div>
      <div class="row" style="padding-top: 3%; padding-bottom: 3%; justify-content: center;">
        <?php
          if(mysqli_num_rows($posts)===0){
        ?>
            <div class="alert alert-secondary" role="alert">
              No hay publicaciones para mostrar en estos momentos.
            </div>
        <?php
          }else{
          while($post = mysqli_fetch_object($posts))
          {
            if($post->img == 'NULL' || $post->img == 'null'){
              $imagen = "http://via.placeholder.com/1024x360";
            }else{
              $imagen = $post->img;
            }
            ?>
        <div class="col-lg-4">
          <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="<?php echo $imagen; ?>" alt="Card image cap">
            <div class="card-body">
              <h5 class="card-title"><?php echo $post->title;?></h5>
              <p class="card-text"><?php echo $post->description;?></p>
              <a href="<?php echo BASE_URL.'services/opiniones/tema/?id='.$post->id;?>" class="btn btn-primary">Ver mas</a>
            </div>
          </div>
        </div>
      <?php
     }
    } ?>
      </div>
    </div>
    <?php require('../../footer.php');?>
  </body>
</html>
