<?php
session_start();
unset($_SESSION['SERVICE']);
$_SESSION['TOPIC'] = TRUE;
include('../../../config.php');
include('../../../conexion.php');
$bd = new Conexion;
$bd->conectar();
$postId = $_GET['id'];
$tema = $bd->query('SELECT * FROM posts WHERE status = 1 AND idPost = '.$postId);
$dataTema = mysqli_fetch_object($tema);
$niveles = $bd->query("SELECT * FROM nivel_participaciones;");
if($tema->imagen != null){
  $image = file_get_contents($tema->img);
  $content = base64_encode($image);
  $data = "data:image/png;base64,{$content}";
}else{
  $data = "../../../noimage.jpg";
}
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <?php require('../../../head.php'); ?>
  <body>
  <?php require('../../../menu.php'); ?>
  <div class="container">
    <div class="flex-column" id="denuncia-logo">
      <div class="d-flex justify-content-center">
          <img src="../img/compilacion.png" class="img-fluid" alt="Denuncia service">
      </div>
      <div class="d-flex justify-content-center">
          <h3><?php echo $dataTema->title; ?></h3>
      </div>
      <div class="d-flex justify-content-center">
        <img src="<?php echo $data; ?>" alt="">
      </div>
      <div class="row pt-5 pb-5">
        <div class="col-sm-12">
          <p><?php echo $dataTema->description;?></p>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="container">
    <div id="disqus_thread"></div>
    <!--<div class="row">
      <div class="col-sm-12 col-md-4 col-lg-2 d-flex flex-column align-items-center">
        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
        <p>
          <a class="float-left" href="#"><strong id="CommentUserName"><?php //echo (isset($_SESSION['username']))?$_SESSION['username']:'Anonimo';?></strong></a>
        </p>
      </div>
      <div class="col-sm-12 col-md-4 col-lg-8 d-flex align-items-center">
        <textarea name="txtComment" id="txtComment" class="form-control" rows="3" cols="80"></textarea>
      </div>
      <div class="col-sm-12 col-md-4 col-lg-2 d-flex align-items-center">
        <button type="button" name="button" class="btn btn-primary" onclick="sendComment('<?php //echo (isset($_SESSION['username']))?$_SESSION['username']:'Anonimo';?>', <?php //echo $data->id;?>)">Comentar</button>
      </div>
    </div>-->
  </div>
  <hr>
  <div class="container" id="comments"></div>
  <?php require('../../../footer.php'); ?>
  <?php require('../../../loginModal.php'); ?>
  <?php require('../../../registroModal.php'); ?>
  <?php require('../../../modalAbout.php'); ?>
  <?php require('../../../alertModal.php'); ?>
  <?php require('../../../scripts.php');?>
  </body>
</html>
