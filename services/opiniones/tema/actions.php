<?php
error_reporting(-1); // reports all errors
ini_set("display_errors", "1"); // shows all errors
ini_set("log_errors", 1);
require('../../config.php');
require('../../conexion.php');
$bd = new Conexion;

if(isset($_POST['getComment'])) {
  try {
    $idComment = $_POST['idComment'];
    if($comment = $bd->query("SELECT * FROM comments WHERE idComment = $idComment;")){
      echo json_encode(mysqli_fetch_array($comment));
    }
  } catch(Exception $e) {
    echo $e;
  }
}

if(isset($_POST['comment'])) {
  try {
    $idPost = $_POST['idPost'];
    $date = date("Y-m-d");
    $comment = $_POST['commentText'];
    $bd->query("INSERT INTO comments (idPost, commentDate, comment) VALUES ($idPost, '$date', '$comment')");
    $lastComment = mysqli_insert_id($bd->con);
    if($comment = $bd->query("SELECT * FROM comments WHERE idComments = ".$lastComment)) {
      echo json_encode(mysqli_fetch_array($comment));
    }
  } catch(Exception $e) {
    echo $e;
  }
}

if(isset($_POST['reply'])) {
  try {
    $idComment = $_POST['idComment'];
    $idUser = $_POST['idUser'];
    $replyComment = $_POST['replyComment'];
    $replyDate = date("Y-m-d");
    if($bd->query("INSERT INTO replies (idComment, idUser, replyComment, replyDate) VALUES ($idComment, $idUser, '$replyComment', '$replyDate')")) {
      echo 'replyInserted';
    }
  } catch(Exception $e) {
    echo $e;
  }
}

if(isset($_POST['like'])) {
  try {
    $idComment = $_POST['idComment'];
    $idUser = $_POST['idUser'];
    $likeDate = date("Y-m-d");
    if($bd->query("INSERT INTO likes (idUser, likeDate, idComment) VALUES ($idUser, '$likeDate', $idComment)")) {
      echo 'likeInserted';
    }
  } catch(Exception $e) {
    echo $e;
  }
}

if(isset($_POST['dislike'])) {
  try {
    $idComment = $_POST['idComment'];
    $idUser = $_POST['idUser'];
    $dislikeDate = date("Y-m-d");
    if($bd->query("INSERT INTO likes (idUser, dislikeDate, idComment) VALUES ($idUser, '$dislikeDate', $idComment)")) {
      echo 'dislikeInserted';
    }
  } catch(Exception $e) {
    echo $e;
  }
}
