<div id="registroModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Crear cuenta</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <div class="row">
              <div class="col-lg-10">
                <label for="">Nivel de participación</label>
                <select class="form-control" id="nivelParti" name="">
                  <option value="0">Elija el nivel</option>
                  <?php while($nivel = mysqli_fetch_object($niveles)){ ?>
                  <option value="<?php echo $nivel->idParti; ?>"><?php echo $nivel->participacion; ?></option>
                <?php } ?>
                </select>
              </div>
              <div class="col-lg-2">
                <label for="">Info</label>
                <button type="button" class="btn btn-primary" name="button" id="modalTiposParticipacion"><span class="icon-question"></button>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" class="form-control" id="nombre" aria-describedby="emailHelp" placeholder="Ingrese su nombre">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Apellido</label>
            <input type="text" class="form-control" id="apellido" aria-describedby="emailHelp" placeholder="Ingrese su apellido">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Teléfono</label>
            <input type="text" class="form-control" id="telefono" aria-describedby="emailHelp" placeholder="Ej. 6691 34 87 45">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email</label>
            <input type="email" class="form-control" id="emailRegistro" aria-describedby="emailHelp" placeholder="sucorreo@gmail.com">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Confirmar email</label>
            <input type="email" class="form-control" id="con_email" aria-describedby="emailHelp" placeholder="Repita su email">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Contraseña</label>
            <input type="password" class="form-control" id="passRegistro" aria-describedby="emailHelp" placeholder="Ingrese a su contraseña">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Confirmar contraseña</label>
            <input type="password" class="form-control" id="con_pass" aria-describedby="emailHelp" placeholder="Repita su contraseña">
          </div>
          <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="terminos">
            <label class="form-check-label" for="exampleCheck1"><a target="_blank" href="pdf/privacidad.pdf">Acepto los terminos y condiciones</a></label>
          </div>
      </form>
      <div class="row">
        <div class="col-md-12" id="messageRegister"></div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnCancelar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        <button type="button" id="btnRegistrar" class="btn btn-primary" onclick="insertAccount()">Registrar</button>
      </div>
    </div>
  </div>
</div>
