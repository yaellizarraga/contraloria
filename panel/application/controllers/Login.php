<?
class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->helper('url');
		$this->load->library('session');
	}

	function index(){
        if($this->session->userdata('sessionExpired') == "0"){
            $this->load->view('panel_view');
        }else{
            $this->load->view('login_view');
        }
    }
    
    function login(){
        $user = $_POST['user'];
        $password = $_POST['pass'];
        $info = $this->Login_model->login($user, $this->convertPassword($password));
        $isValid = false;
        if(count($info) > 0){
            $isValid = true;
        }
        if($isValid){
            $now = date("Y-m-d H:i:s");
            $session_data = [
                'user' => $info->idUser,
                'creation' => $now,
                'sessionExpired' => 0
            ];
            $this->session->set_userdata($session_data);
        }
        echo json_encode($isValid);
    }

    function convertPassword($password){
        $newPass = hash('sha256',"c0ntrA10r1a.%mazatlan".hash('sha256', $password));
        return $newPass;
    }

    function closeSession(){
        $isValid = false;
        $this->session->sess_destroy();
        $isValid = true;
        echo json_encode($isValid);
    }
}
?>