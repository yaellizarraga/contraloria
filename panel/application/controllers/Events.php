<?
class Events extends CI_Controller{
    function __construct(){
        parent::__construct();
		$this->load->model('events_model');
		$this->load->library('valid');
        $this->load->library('upload_file');
        $this->load->helper('url');
		$this->load->library('session');
	}

	public function index(){
        $this->load->view('event_view');
    }

    public function getEventsTable(){
        echo json_encode($this->events_model->getEventsTable($_POST));
    }

    public function toggleEvents(){
        echo json_encode($this->events_model->toggleEvent($_POST['idEvent']));
    }

    public function getEventsInfo(){
        echo json_encode($this->events_model->getEvent($_POST['idEvent']));
    }
    
    public function saveEvent(){
        if(!empty($_POST)){
            $data = $_POST;
            if(!isset($data['idEvent'])){
                $data['idEvent'] = 0;
            }
            $idEvent = $data['idEvent'];
            unset($data['idEvent']);
            $idEvent = $this->events_model->saveEvent($idEvent, $data);
            $uploads = [];
            $urlStart = explode("panel",FCPATH);
            $resources_folder = $urlStart[0]."panel_storage";
            if(!file_exists($resources_folder)){
				if (!mkdir($resources_folder)) {
                    echo json_encode(["msg" => "Falló al crear la carpeta de recursos"]);
                    die();
				}
			}
            if(!empty($_FILES)){
                $resources = $_FILES;
                $folder = "";
                $resourceType = "";
                foreach ($resources as $key => $val) {
                    $type = explode("/",$val['type']);
                    switch ($type[0]) {
                        case 'image':
                            $folder = "$resources_folder/images";
                            $resourceType = "Image";
                        break;
                        default:
                            $folder = "";
                            $resourceType = "";
                        break;
                    }
                    if($folder != "" && $resourceType != ""){
                        $upload = $this->upload_file->upload_resources($key,$idEvent,$folder,'image','event');
                        $uploads[] = $upload;
                        if($upload['status'] == 1){
                            $save_upload = [
                                "idEvent" => $idEvent,
                                "resourceUrl" => $upload['url'],
                                "resourceType" => $resourceType
                            ];
                            $resource = $this->events_model->saveEventResource($save_upload);
                        }
                    }else{
                        $uploads[] = ["Archivo subido no es del tipo correcto."];
                    }
                }
            }
            //echo "<pre>";
            echo json_encode([
                "status" => 1,
                "files" => $uploads
            ]);
            //echo "</pre>";
        }else{
            echo json_encode(["msg" => "No data"]);
        }
    }
}
?>