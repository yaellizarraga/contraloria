<?
class Posts extends CI_Controller{
    function __construct(){
        parent::__construct();
		$this->load->model('posts_model');
		$this->load->library('valid');
        $this->load->library('upload_file');
        $this->load->helper('url');
		$this->load->library('session');
	}

	public function index(){
        $this->load->view('posts_view');
    }

    public function getPostsTable(){
        echo json_encode($this->posts_model->getPostsTable($_POST));
    }

    public function togglePosts(){
        echo json_encode($this->posts_model->togglePosts($_POST['idPost']));
    }

    public function getPostInfo(){
        echo json_encode($this->posts_model->getPostInfo($_POST['idPost']));
    }
    
    public function savePosts(){
        if(!empty($_POST)){
            $data = $_POST;
            if(!isset($data['idPost'])){
                $data['idPost'] = 0;
            }
            $idPost = $data['idPost'];
            unset($data['idPost']);
            $data['idUser'] = $this->session->userdata('user');
            $idPost = $this->posts_model->savePosts($idPost, $data);
            $uploads = [];
            $urlStart = explode("panel",FCPATH);
            $resources_folder = $urlStart[0]."panel_storage";
            if(!file_exists($resources_folder)){
				if (!mkdir($resources_folder)) {
                    echo json_encode(["msg" => "Falló al crear la carpeta de recursos"]);
                    die();
				}
			}
            if(!empty($_FILES)){
                $resources = $_FILES;
                $folder = "";
                $resourceType = "";
                foreach ($resources as $key => $val) {
                    $type = explode("/",$val['type']);
                    switch ($type[0]) {
                        case 'image':
                            $folder = "$resources_folder/images";
                            $resourceType = "Image";
                        break;
                        default:
                            $folder = "";
                            $resourceType = "";
                        break;
                    }
                    if($folder != "" && $resourceType != ""){
                        $upload = $this->upload_file->upload_resources($key,$idPost,$folder,'image','posts');
                        $uploads[] = $upload;
                        if($upload['status'] == 1){
                            $save_upload = [
                                "idPost" => $idPost,
                                "resourceUrl" => $upload['url'],
                                "resourceType" => $resourceType
                            ];
                            $resource = $this->posts_model->savePostsResource($save_upload);
                        }
                    }else{
                        $uploads[] = ["Archivo subido no es del tipo correcto."];
                    }
                }
            }
            //echo "<pre>";
            echo json_encode([
                "status" => 1,
                "files" => $uploads
            ]);
            //echo "</pre>";
        }else{
            echo json_encode(["msg" => "No data"]);
        }
    }
}
?>