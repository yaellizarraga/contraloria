<?
class Mobile extends CI_Controller{
    function __construct(){
        parent::__construct();
		$this->load->model('mobile_model');
	}

	public function index(){
        $this->load->view('event_view');
    }

    public function login(){
        $user = isset($_POST["user"])?$_POST["user"]:null;
        $pass = isset($_POST["pass"])?$_POST["pass"]:null;
        $res = [
            "status" => 0,
            "msg" => "Falta información para proceder",
            "error" => ""
        ];
        if(isset($user) && isset($pass) ){
            $user_login = $this->mobile_model->login($user, $pass);
            if(count($user_login) > 0 ){
                $res["status"] = 1;
                $res["msg"] = "Usuario válido";
            }else{
                $res["status"] = 0;
                $res["msg"] = "Usuario y/o contraseña incorrectos";
            }
        }else{
            
        }
        echo json_encode($res);
    }

}
?>