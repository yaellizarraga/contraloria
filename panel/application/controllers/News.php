<?
class News extends CI_Controller{
    function __construct(){
        parent::__construct();
		$this->load->model('news_model');
		$this->load->library('valid');
        $this->load->library('upload_file');
        $this->load->helper('url');
		$this->load->library('session');
	}

	public function index(){
        $this->load->view('news_view');
    }

    public function getNewsTable(){
        echo json_encode($this->news_model->getNewsTable($_POST));
    }

    public function toggleNews(){
        echo json_encode($this->news_model->toggleNews($_POST['idNews']));
    }

    public function getNewsInfo(){
        echo json_encode($this->news_model->getNews($_POST['idNews']));
    }
    
    public function saveNews(){
        if(!empty($_POST)){
            $data = $_POST;
            if(!isset($data['idNews'])){
                $data['idNews'] = 0;
            }
            $idNews = $data['idNews'];
            unset($data['idNews']);
            $idNews = $this->news_model->saveNews($idNews, $data);
            $uploads = [];
            $urlStart = explode("panel",FCPATH);
            $resources_folder = $urlStart[0]."panel_storage";
            if(!file_exists($resources_folder)){
				if (!mkdir($resources_folder)) {
                    echo json_encode(["msg" => "Falló al crear la carpeta de recursos"]);
                    die();
				}
			}
            if(!empty($_FILES)){
                $resources = $_FILES;
                $folder = "";
                $resourceType = "";
                foreach ($resources as $key => $val) {
                    $type = explode("/",$val['type']);
                    switch ($type[0]) {
                        case 'image':
                            $folder = "$resources_folder/images";
                            $resourceType = "Image";
                        break;
                        default:
                            $folder = "";
                            $resourceType = "";
                        break;
                    }
                    if($folder != "" && $resourceType != ""){
                        $upload = $this->upload_file->upload_resources($key,$idNews,$folder,'image','news');
                        $uploads[] = $upload;
                        if($upload['status'] == 1){
                            $save_upload = [
                                "idNews" => $idNews,
                                "resourceUrl" => $upload['url'],
                                "resourceType" => $resourceType
                            ];
                            $resource = $this->news_model->saveNewsResource($save_upload);
                        }
                    }else{
                        $uploads[] = ["Archivo subido no es del tipo correcto."];
                    }
                }
            }
            //echo "<pre>";
            echo json_encode([
                "status" => 1,
                "files" => $uploads
            ]);
            //echo "</pre>";
        }else{
            echo json_encode(["msg" => "No data"]);
        }
    }
}
?>