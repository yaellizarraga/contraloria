<?
class Users extends CI_Controller{
    function __construct(){
        parent::__construct();
		$this->load->model('users_model');
		$this->load->helper('url');
		$this->load->library('session');
	}

	public function index(){
        $data = [];
        $this->load->view('users_view',$data);
    }
    
    public function getUserTable(){
        echo json_encode($this->users_model->getUsers($_POST));
    }

    public function saveUser(){
        $_POST['pass'] = $this->convertPassword(trim($_POST['pass']));
        echo json_encode($this->users_model->saveUser($_POST));
    }

    public function toggleUser(){
        echo json_encode($this->users_model->toggleUser($_POST['idUser']));
    }

    public function getUserInfo(){
        echo json_encode($this->users_model->getUserInfo($_POST['idUser']));
    }

    function convertPassword($password){
        $newPass = hash('sha256',"c0ntrA10r1a.%mazatlan".hash('sha256', $password));
        return $newPass;
    }
}
?>