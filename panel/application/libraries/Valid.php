<?
defined('BASEPATH') OR exit('No direct script access allowed');

class Valid {
	public function required($name){
		if(isset($_POST[$name])){
			$value = trim($_POST[$name]); //remueve espacios al inicio y fin de la cadena
			if($value != ""){
			$value = htmlspecialchars($_POST[$name]); //reemplaza caracteres especiales con entidades HTML
			$value = stripslashes($value); //remueve diagonales
			return $value;
			}
		}
		return false;
	}
	public function number($name){
		if(isset($_POST[$name])){
			$value = trim($_POST[$name]);
			if(is_numeric($value)){
			return $value;
			}
		}
		return false;
	}
	
	public function email($name){
		if(isset($_POST[$name])){
			$value = trim($_POST[$name]);
			if(filter_var($value, FILTER_VALIDATE_EMAIL)){
			return $value;
			}
		}
		return false;
	}
	
	public function img_file($name){
		if(!empty($_FILES[$name]) and $_FILES[$name]['error']==0){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function is_unique($buscar,$campo,$id=""){
		if(!empty(trim($_POST["$campo"]))){
			
		}else{
			return true;
		}		
	}
	
	public function validate($rules,$id=""){
		$mensajes = [
			'required'=>'<p>El campo {0} es requerido</p>',
			'number'=>'<p>El campo {0} debe ser numerico<p>',
			'email'=>'<p>El campo {0} no es un correo válido<p>',
			'is_unique'=>'<p>El campo {0} ya fue registrado anteriormente favor de ingresar otro<p>',
			'img_file'=>'<p>Se debe subir una imagen<p>',				
		];
		$errors="";
		$campos = [];
		if(!empty($_POST)){
		foreach ($rules as $campo => $validaciones) {
			$reglas = explode('|', $validaciones);
			foreach ($reglas as $regla) {
				 if(is_callable($regla) or strpos($regla,'is_unique')!==false){//Verifica si la cadena se puede usar como funcion
					 if ($regla=='md5') {
						 $valid = md5($_POST[$campo]);
					 }else if(strpos($regla,'is_unique')!== false){
							if (strpos($regla,'[') !== false and strpos($regla,']') !== false) {
								$inicio=strpos($regla,'[')+1;
								$fin=strpos($regla,']');
								if($inicio===$fin){
									$mensajes['is_unique']='<p>La regla is_unique necesita tener el formato de ->is_unique[tabla.campo]<p>';
									$valid=false;
								}else{
									$buscar=substr($regla, $inicio, $fin - $inicio);
									if($id){
										$valid = is_unique($buscar,$campo,$id);
									}else
										$valid = is_unique($buscar,$campo);
										if(!is_bool($valid) and $valid!==$_POST["$campo"]){
											$mensajes['is_unique']="<p>$valid<p>";
											$valid=false;
										}
								}
							}else{
								$mensajes['is_unique']='<p>La regla is_unique necesita tener el formato de ->is_unique[tabla.campo]<p>';
								$valid=false;	
							}		 			
					 }else
					 $valid = $regla($campo);
					 if(!$valid){
						 if (strpos($regla,'is_unique')!==false) {
						 $errors.=str_replace("{0}",$campo,$mensajes['is_unique']);	
						 }else
						 $errors.=str_replace("{0}",$campo,$mensajes[$regla]);
					 }
					 else{
						 $_POST[$campo]=$valid;
						 $campos[$campo] = $valid;
					 }
				 }
			 }
		  } 
		}
		return [
			'status'=>($errors=="" and !empty($_POST)),
			'values'=>$campos,
			'errors'=>$errors
		];
	}
}