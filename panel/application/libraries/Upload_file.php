<?
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_file{
	private $CI;

	function __construct(){
        $this->CI = get_instance();
	}
	
	public function upload_resources($field,$idValue,$carpeta,$extensions,$resource_category){
		$response = [
			"status" => 0,
			"msg" => "Error inesperado.",
			"url" => ""
		];
		$valid = true;
		if($_FILES[$field]['error'] == 0){
			if(!file_exists($carpeta)){
				if (!mkdir($carpeta)) {
					$valid = false;
					$response['msg'] = "Error al crear la carpeta $carpeta";
					$response['test'] = !file_exists($carpeta);
				}
			}
			if($valid === true){
				$valid_extensions = explode('|', $extensions);
				$type = explode("/",$_FILES[$field]['type']);
				$resource = $_FILES[$field]['name'];
				$resource_ext = explode('.', $resource);
				$resource_ext = $resource_ext[count($resource_ext)-1];
				if(in_array($type[0],$valid_extensions)){
					$resource = "$carpeta/".$resource_category.$idValue.time().".$resource_ext";
					$final_url = explode("panel_storage/",$resource);
					if (move_uploaded_file($_FILES[$field]['tmp_name'], $resource)) {
						$response['status'] = 1;
						$response['msg'] = "Archivo subido correctamente";
						$response['url'] = $final_url[1];
					} else {
						$response['msg'] = "Error al mover el archivo subido";
					}
				}else{
					$response['msg'] = "La extension del archivo no coincide con las permitidas en los parametros, es de tipo $type";
				}
			}
		}else{
			$response['msg'] = "Ocurrio un error al subir el archivo ->numero de error:".$_FILES[$field]['error'];
		}
		return $response;
	}
	
}
?>