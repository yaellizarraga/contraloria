<?
class Users_model extends CI_Model {
	public function __construct(){
        $this->load->database();
    }

    public function saveUser($data){
        $save = [
            "user" => trim($data['user']),
            "password" => $data['pass']
        ];
        if($data['idUser'] == 0){
            $this->db->insert('users_panel',$save);
            $id = $this->db->insert_id();
        }else{
            $this->db->where('idUser', $data['idUser']);
            $this->db->update('users_panel', $save);
            $id = $data['idUser'];
        }
        return $id;
    }

    public function toggleUser($idUser){
        $this->db->query("
            update users_panel
            set status = !status
            where idUser = $idUser
        ");
        return $idUser;
    }

    public function getUserInfo($idUser){
        $this->db->where('idUser',$idUser);
        return $this->db->get('users_panel')->row();
    }
    
	public function getUsers($data){
        $colSearch = ["user", "status"];
		$searchBox = $data['search']['value'];
		$info = new stdClass();
		$info->draw = $data['draw'];	
		$limit = $data['length'];
		$offset = $data['start'];
		$columnOrder = $data['order'][0]['column'];
		$columnOrderDirection = $data['order'][0]['dir'];
        $searchFilter = '';
        $this->db->start_cache();
        if(trim($searchBox) != ''){
            $pieces = explode(" ", $searchBox);
            foreach ($pieces as $keyPiece => $textPiece) {
                if($keyPiece == 0){
                    $searchFilter.='(';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter.="$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter.=" OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }else{
                    $searchFilter .= ' AND (';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter .= "$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter .= " OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }
            }
            $this->db->where($searchFilter);
        }
        switch ($columnOrder) {
            case 0:
		        $this->db->order_by('idUser',$columnOrderDirection);
		    break;
		    case 1:
		        $this->db->order_by('user',$columnOrderDirection);
		    break;
			case 2:
		        $this->db->order_by('status',$columnOrderDirection);
		    break;
		    default:
		    	$this->db->order_by('idUser',"asc");
		    break;    			

        }
        $this->db->select("idUser, user, if(status = 1, 'Activo', 'Desactivado') as status, status as statusNumber");
        $this->db->stop_cache();
        $this->db->limit($limit,$offset);
        $info->recordsFiltered = count($this->db->get("users_panel")->result());
        $users_record = $this->db->get("users_panel")->result();
        $info->recordsTotal = count($users_record);
        foreach ($users_record as $key => $record) {
			$final_info[$key][] = $record->idUser;
			$final_info[$key][] = $record->user;
            $final_info[$key][] = $record->status;
            if($record->statusNumber){
                $final_info[$key][] = "<button class='btn btn-outline-danger btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idUser)'><i class='fa fa-user-times' aria-hidden='true'></i></button>";
            }else{
                $final_info[$key][] = "<button class='btn btn-outline-success btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idUser)'><i class='fa fa-user-check' aria-hidden='true'></i></button>";
            }
			$final_info[$key][] = "<button class='btn btn-outline-primary btnBootstrap' onclick='openEditModal($record->idUser)'><i class='fa fa-user-edit' aria-hidden='true'></i></button>";
		}
		if(isset($final_info)){
			$info->data = $final_info;
		}else{
			$info->data = new stdClass();
        }
        $this->db->flush_cache();
		return $info;
	}

}
?>