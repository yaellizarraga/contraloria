<?
class Posts_model extends CI_Model {
	public function __construct(){
        $this->load->database();
    }

	function getPostInfo($idPost){
		$this->db->where('idPost',$idPost);
		return $this->db->get('posts')->row();
    }

    public function togglePosts($idPost){
        $this->db->query("
            update posts
            set status = !status
            where idPost = $idPost
        ");
        return $idPost;
    }

	function savePosts($idPost, $data){
        if($idPost > 0){
            $this->db->where('idPost',$idPost);
            $result = $this->db->update("posts",$data);
            return $idPost;
        }else{
            $result = $this->db->insert("posts",$data);
            return $this->db->insert_id();
        }
    }
    
    function savePostsResource($data){
        $this->db->where('idPost',$data['idPost']);
        $this->db->update("posts",['img' => $data['resourceUrl']]);
        return $data['idPost'];
    }

    public function getPostsTable($data){
        $colSearch = ["title", "date"];
		$searchBox = $data['search']['value'];
		$info = new stdClass();
		$info->draw = $data['draw'];	
		$limit = $data['length'];
		$offset = $data['start'];
		$columnOrder = $data['order'][0]['column'];
		$columnOrderDirection = $data['order'][0]['dir'];
        $searchFilter = '';
        $this->db->start_cache();
        if(trim($searchBox) != ''){
            $pieces = explode(" ", $searchBox);
            foreach ($pieces as $keyPiece => $textPiece) {
                if($keyPiece == 0){
                    $searchFilter.='(';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter.="$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter.=" OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }else{
                    $searchFilter .= ' AND (';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter .= "$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter .= " OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }
            }
            $this->db->where($searchFilter);
        }
        switch ($columnOrder) {
            case 0:
		        $this->db->order_by('idPost',$columnOrderDirection);
		    break;
		    case 1:
		        $this->db->order_by('title',$columnOrderDirection);
		    break;
			case 3:
		        $this->db->order_by('date',$columnOrderDirection);
		    break;
			case 4:
		        $this->db->order_by('status',$columnOrderDirection);
		    break;
		    default:
		    	$this->db->order_by('idPost',"desc");
		    break;    			

        }
        $this->db->select("idPost, title, description, date, if(status = 1, 'Activo', 'Desactivado') as status, status as statusNumber");
        $this->db->stop_cache();
        $this->db->limit($limit,$offset);
        $users_record = $this->db->get("posts")->result();
        $info->recordsFiltered = count($this->db->get("posts")->result());
        $info->recordsTotal = count($users_record);
        foreach ($users_record as $key => $record) {
			$final_info[$key][] = $record->idPost;
			$final_info[$key][] = $record->title;
            $final_info[$key][] = $record->description;
            $final_info[$key][] = $record->date;
            $final_info[$key][] = $record->status;
            if($record->statusNumber){
                $final_info[$key][] = "<button class='btn btn-outline-danger btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idPost)'><i class='fa fa-times-circle' aria-hidden='true'></i></button>";
            }else{
                $final_info[$key][] = "<button class='btn btn-outline-success btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idPost)'><i class='fa fa-check-circle' aria-hidden='true'></i></button>";
            }
			$final_info[$key][] = "<button class='btn btn-outline-primary btnBootstrap' onclick='openEditModal($record->idPost)'><i class='fa fa-edit' aria-hidden='true'></i></button>";
		}
		if(isset($final_info)){
			$info->data = $final_info;
		}else{
			$info->data = new stdClass();
        }
        $this->db->flush_cache();
		return $info;
	}
}
?>