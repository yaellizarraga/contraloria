<?
class News_model extends CI_Model {
	public function __construct(){
        $this->load->database();
    }
    
	function getAllNews(){
        $this->db->where('status',1);
		return $this->db->get('news')->result();
	}

	function getNews($idNews){
		$this->db->where('idNews',$idNews);
		return $this->db->get('news')->row();
    }

    public function toggleNews($idNews){
        $this->db->query("
            update news
            set status = !status
            where idNews = $idNews
        ");
        return $idNews;
    }
    
    function getTodayNews(){
		$this->db->where('DATE(date) = DATE(NOW()) and status = 1');
		$datos = $this->db->get('news');
		return $datos->fila();
	}

	function saveNews($idNews, $data){
        if($idNews > 0){
            $this->db->where('idNews',$idNews);
            $result = $this->db->update("news",$data);
            return $idNews;
        }else{
            $result = $this->db->insert("news",$data);
            return $this->db->insert_id();
        }
    }
    
    function saveNewsResource($data){
        $this->db->where('idNews',$data['idNews']);
        $this->db->update("news",['image' => $data['resourceUrl']]);
        return $data['idNews'];
    }

    public function getNewsTable($data){
        $colSearch = ["title", "date"];
		$searchBox = $data['search']['value'];
		$info = new stdClass();
		$info->draw = $data['draw'];	
		$limit = $data['length'];
		$offset = $data['start'];
		$columnOrder = $data['order'][0]['column'];
		$columnOrderDirection = $data['order'][0]['dir'];
        $searchFilter = '';
        $this->db->start_cache();
        if(trim($searchBox) != ''){
            $pieces = explode(" ", $searchBox);
            foreach ($pieces as $keyPiece => $textPiece) {
                if($keyPiece == 0){
                    $searchFilter.='(';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter.="$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter.=" OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }else{
                    $searchFilter .= ' AND (';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter .= "$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter .= " OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }
            }
            $this->db->where($searchFilter);
        }
        switch ($columnOrder) {
            case 0:
		        $this->db->order_by('idNews',$columnOrderDirection);
		    break;
		    case 1:
		        $this->db->order_by('title',$columnOrderDirection);
		    break;
			case 3:
		        $this->db->order_by('date',$columnOrderDirection);
		    break;
			case 4:
		        $this->db->order_by('status',$columnOrderDirection);
		    break;
		    default:
		    	$this->db->order_by('idNews',"desc");
		    break;    			

        }
        $this->db->select("idNews, title, content, date, if(status = 1, 'Activo', 'Desactivado') as status, status as statusNumber");
        $this->db->stop_cache();
        $this->db->limit($limit,$offset);
        $users_record = $this->db->get("news")->result();
        $info->recordsFiltered = count($this->db->get("news")->result());
        $info->recordsTotal = count($users_record);
        foreach ($users_record as $key => $record) {
			$final_info[$key][] = $record->idNews;
			$final_info[$key][] = $record->title;
            $final_info[$key][] = $record->content;
            $final_info[$key][] = $record->date;
            $final_info[$key][] = $record->status;
            if($record->statusNumber){
                $final_info[$key][] = "<button class='btn btn-outline-danger btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idNews)'><i class='fa fa-times-circle' aria-hidden='true'></i></button>";
            }else{
                $final_info[$key][] = "<button class='btn btn-outline-success btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idNews)'><i class='fa fa-check-circle' aria-hidden='true'></i></button>";
            }
			$final_info[$key][] = "<button class='btn btn-outline-primary btnBootstrap' onclick='openEditModal($record->idNews)'><i class='fa fa-edit' aria-hidden='true'></i></button>";
		}
		if(isset($final_info)){
			$info->data = $final_info;
		}else{
			$info->data = new stdClass();
        }
        $this->db->flush_cache();
		return $info;
	}
}
?>