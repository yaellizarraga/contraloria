<?
class Events_model extends CI_Model {
	public function __construct(){
        $this->load->database();
    }
    
	function getEvents(){
        $this->db->where('status',1);
		$datos = $this->db->get('events');
		return $datos->filas();
	}

	function getEvent($idEvent){
        $this->db->where('idEvent',$idEvent);
		return $this->db->get('events')->row();
    }
    
    function getTodayEvents(){
		$this->db->where('DATE(date) = DATE(NOW()) and status = 1');
		$datos = $this->db->get('events');
		return $datos->fila();
    }
    
    public function toggleevents($idEvent){
        $this->db->query("
            update events
            set status = !status
            where idEvent = $idEvent
        ");
        return $idEvent;
    }

	function saveEvent($idEvent, $data){
        if($idEvent > 0){
            $this->db->where('idEvent',$idEvent);
            $result = $this->db->update("events",$data);
            return $idEvent;
        }else{
            $result = $this->db->insert("events",$data);
            return $this->db->insert_id();
        }
    }
    
    function saveEventResource($data){
        $this->db->where('idEvent',$data['idEvent']);
        $this->db->update("events ",['image' => $data['resourceUrl']]);
        return $data['idEvent'];
    }

    public function getEventsTable($data){
        $colSearch = ["title", "date"];
		$searchBox = $data['search']['value'];
		$info = new stdClass();
		$info->draw = $data['draw'];	
		$limit = $data['length'];
		$offset = $data['start'];
		$columnOrder = $data['order'][0]['column'];
		$columnOrderDirection = $data['order'][0]['dir'];
        $searchFilter = '';
        $this->db->start_cache();
        if(trim($searchBox) != ''){
            $pieces = explode(" ", $searchBox);
            foreach ($pieces as $keyPiece => $textPiece) {
                if($keyPiece == 0){
                    $searchFilter.='(';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter.="$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter.=" OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }else{
                    $searchFilter .= ' AND (';
                    foreach ($colSearch as $key => $value) {
                        if($key == 0){
                            $searchFilter .= "$value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }else{
                            $searchFilter .= " OR $value LIKE '%".$this->db->escape_like_str($textPiece)."%'";
                        }
                    }
                    $searchFilter .= ')';
                }
            }
            $this->db->where($searchFilter);
        }
        switch ($columnOrder) {
            case 0:
		        $this->db->order_by('idEvent',$columnOrderDirection);
		    break;
		    case 1:
		        $this->db->order_by('title',$columnOrderDirection);
		    break;
			case 3:
		        $this->db->order_by('date',$columnOrderDirection);
		    break;
			case 4:
		        $this->db->order_by('status',$columnOrderDirection);
		    break;
		    default:
		    	$this->db->order_by('idEvent',"desc");
		    break;    			

        }
        $this->db->select("idEvent, title, content, date, if(status = 1, 'Activo', 'Desactivado') as status, status as statusNumber");
        $this->db->stop_cache();
        $this->db->limit($limit,$offset);
        $users_record = $this->db->get("events")->result();
        $info->recordsFiltered = count($this->db->get("events")->result());
        $info->recordsTotal = count($users_record);
        foreach ($users_record as $key => $record) {
			$final_info[$key][] = $record->idEvent;
			$final_info[$key][] = $record->title;
            $final_info[$key][] = $record->content;
            $final_info[$key][] = $record->date;
            $final_info[$key][] = $record->status;
            if($record->statusNumber){
                $final_info[$key][] = "<button class='btn btn-outline-danger btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idEvent)'><i class='fa fa-times-circle' aria-hidden='true'></i></button>";
            }else{
                $final_info[$key][] = "<button class='btn btn-outline-success btnBootstrap' onclick='toggleConfirm($record->statusNumber,$record->idEvent)'><i class='fa fa-check-circle' aria-hidden='true'></i></button>";
            }
			$final_info[$key][] = "<button class='btn btn-outline-primary btnBootstrap' onclick='openEditModal($record->idEvent)'><i class='fa fa-edit' aria-hidden='true'></i></button>";
		}
		if(isset($final_info)){
			$info->data = $final_info;
		}else{
			$info->data = new stdClass();
        }
        $this->db->flush_cache();
		return $info;
	}
}
?>