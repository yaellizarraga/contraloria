<?
class Session_Model extends CI_Model{
    public function __construct(){
        $this->load->database();
        $this->load->helper('url');
		$this->load->library('session');
    }

    public function verifySession(){
        if($this->session->userdata('sessionExpired') == "0"){
            $session_data = [
                'sessionExpired' => 0,
            ];
            $this->session->set_userdata('sessionExpired', 0);
            return 1;
        }else{
            $this->session->sess_destroy();
            return 0;
        }
    }

}
?>