<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PANEL DE ADMINISTRACION CONCIMAZ | CONTRALORIA CIUDADANA MAZATLAN</title>
    <link rel="icon" href="../favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700|Anton" rel="stylesheet">
    <link rel="stylesheet" href="../fonts/icomoon/style.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="../css/aos.css">
    <link rel="stylesheet" href="../css/style.css">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
    <style>
        ul.panel .nav-item.active{
            background-color: #e9ecef33;
        }
        .swal2-actions.swal2-loading .swal2-styled.swal2-confirm{
            border-color: transparent !important;
            border-left-color: rgb(158, 96, 180) !important;
            border-right-color: rgb(158, 96, 180) !important;
        }
    </style>
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="./">PANEL DE ADMINISTRACION</a>
        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="sidebar navbar-nav panel">
            <li class="nav-item active">
                <a class="nav-link" href="./">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Inicio</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link panel_section" href="<?=base_url()?>users" data-title="Usuarios">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Usuarios</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link panel_section" href="<?=base_url()?>News" data-title="Noticias">
                    <i class="fas fa-fw fa-newspaper"></i>
                    <span>Noticias</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link panel_section" href="<?=base_url()?>Events" data-title="Eventos">
                    <i class="fas fa-fw fa-calendar-alt"></i>
                    <span>Eventos</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link panel_section" href="<?=base_url()?>Posts" data-title="Temas Posteados">
                    <i class="fas fa-fw fa-clipboard"></i>
                    <span>Temas</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link close_session" href="#" data-title="Cerrar Sesión">
                    <i class="fas fa-fw fa-sign-out-alt"></i>
                    <span>Cerrar Sesión</span>
                </a>
            </li>
        </ul>
        <div id="content-wrapper">
            <!-- /.container-fluid -->
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="./">Panel</a>
                    </li>
                    <li class="breadcrumb-item active" id="panel_breadcrumb">Inicio</li>
                </ol>
            </div>
            <!-- /.container-fluid -->
            <!-- /.container-fluid -->
            <div id="panel_content" class="container-fluid">
            </div>
            <!-- /.container-fluid -->
            <!-- Sticky Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                    <span>Copyright ©2019 Todos los derechos reservados | CONCIMAZ</span>
                    </div>
                </div>
            </footer>
            <!-- Sticky Footer -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'>

    <script src="../js/sb-admin.min.js"></script>
    <script type="text/javascript">
        $("a.panel_section").click(function(e){
            Swal.fire({
                title: 'Cargando ...',
                text: 'Espere un momento por favor.',
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
            });
            let section = $(this);
            let url = $(section).attr("href");
            let title = $(section).attr("data-title");
            e.preventDefault();
            $.ajax({
                type: "get",
                url: url,
                success: function (res) {
                    $("#panel_breadcrumb").text(title);
                    $("ul.panel .nav-item.active").removeClass("active");
                    $(section).parent().addClass('active');
                    $("#panel_content").html(res);
                    Swal.close();
                }
            });
        });

        $("a.close_session").click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Confirmación',
                text: "¿Desea cerrar sesión?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continuar',
                cancelButtonText: 'Cancelar'
            }).then((result) => {
                if (result.value) {
                    closeSession();
                }
            });
        });

        function closeSession(){
            Swal.close();
            Swal.fire({
                title: 'Cargando ...',
                text: 'Espere un momento por favor.',
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
            });
            $.ajax({
                type: "get",
                url: "<?=base_url()?>Login/closeSession"
            })
            .done(function (res) {
                res = JSON.parse(res);
                Swal.close();
                if(res){
                    location.reload(true);
                }else{
                    Swal.fire({
                        title: 'Error',
                        text: 'Ha ocurrido un error inesperado al cerrar sesión, favor de recargar la página',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                }
            })
            .fail(function(jqXHR, textStatus){
                Swal.close();
                Swal.fire({
                    title: 'Error',
                    text: 'Ha ocurrido un error, favor de intentarlo nuevamente',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            });
        }
    </script>
</body>
</html>