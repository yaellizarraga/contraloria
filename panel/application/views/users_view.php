<div class="container">
    <div class="row justify-content-center">
        <div class="col-8 text-center">
            <h1>Usuarios</h1>
        </div>
    </div>
    <div class="row justify-content-right">
        <div class="form-group col">
            <button class="btn btn-outline-primary btnBootstrap float-right" onclick="openUserModal();"><i class="fas fa-fw fa-user-plus"></i>Añadir usuario</button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="table-responsive">
                <table id="users" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Activar/Desactivar</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="userModal" tabindex="-1" role="dialog" aria-labelledby="userModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userModalLabel">Usuario</h5>
                <button type="button" class="close btnBootstrap" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <input id="user" type="text" class="hidden" value=0 hidden>
                    <div class="form-group col-md-12">
                        <label for="panelNewUser"><b>Usuario</b></label>
                        <input id="panelNewUser" type="text" class="form-control" placeholder="Ingrese el usuario">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="panelNewPass"><b>Contraseña</b></label>
                        <div class="input-group mb-3" id="panelNewPassGroup">
                            <input id="panelNewPass" type="password" class="form-control" placeholder="Ingrese la contraseña">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary btnBootstrap" type="button" onclick="showPass()">
                                    <i class="fa fa-eye-slash" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary btnBootstrap" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="saveUser()" class="btn btn-outline-success btnBootstrap">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var userTable;
    function openUserModal(){
        $("#user").val(0);
        $("#panelNewUser").val("");
        $("#panelNewPass").val("");
        $('#userModal').modal('show');
    }

    function openEditModal(user){
        Swal.fire({
            title: 'Cargando ...',
            text: 'Espere un momento por favor.',
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        $.ajax({
            type: "post",
            url: "<?=base_url()?>Users/getUserInfo",
            data: {
                idUser: user
            }
        })
        .done(function (res) {
            res = JSON.parse(res);
            Swal.close();
            if(res){
                $("#user").val(user);
                $("#panelNewUser").val(res.user);
                $("#panelNewPass").val("");
                $('#userModal').modal('show');
            }
        })
        .fail(function(jqXHR, textStatus){
            Swal.close();
            Swal.fire({
                title: 'Error',
                text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        });
    }

    function showPass(e){
        event.preventDefault();
        if($('#panelNewPassGroup input').attr("type") == "text"){
            $('#panelNewPassGroup input').attr('type', 'password');
            $('#panelNewPassGroup i').addClass( "fa-eye-slash" );
            $('#panelNewPassGroup i').removeClass( "fa-eye" );
        }else if($('#panelNewPassGroup input').attr("type") == "password"){
            $('#panelNewPassGroup input').attr('type', 'text');
            $('#panelNewPassGroup i').removeClass( "fa-eye-slash" );
            $('#panelNewPassGroup i').addClass( "fa-eye" );
        }
    }

    function saveUser(){
        $('#userModal').modal('hide');
        Swal.fire({
            title: 'Cargando ...',
            text: 'Espere un momento por favor.',
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        $.ajax({
            type: "post",
            url: "<?=base_url()?>Users/saveUser",
            data: {
                user: $("#panelNewUser").val().trim(),
                pass: $("#panelNewPass").val().trim(),
                idUser: $("#user").val()
            }
        })
        .done(function (res) {
            res = JSON.parse(res);
            Swal.close();
            if(res){
                Swal.fire({
                    title: 'Éxito',
                    text: 'Información guardada correctamente',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
            }else{
                Swal.fire({
                    title: 'Error',
                    text: 'Ha ocurrido un error inesperado al guardar la información',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            }
            userTable.draw();
        })
        .fail(function(jqXHR, textStatus){
            Swal.close();
            Swal.fire({
                title: 'Error',
                text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
            $('#userModal').modal('show');
        });
    }

    function toggleUser(user = 0){
        Swal.close();
        Swal.fire({
            title: 'Cargando ...',
            text: 'Espere un momento por favor.',
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        $.ajax({
            type: "post",
            url: "<?=base_url()?>Users/toggleUser",
            data: {
                idUser: user
            }
        })
        .done(function (res) {
            res = JSON.parse(res);
            Swal.close();
            if(res){
                Swal.fire({
                    title: 'Éxito',
                    text: 'Cambio guardado correctamente',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
            }else{
                Swal.fire({
                    title: 'Error',
                    text: 'Ha ocurrido un error inesperado al guardar el cambio',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            }
            userTable.draw();
        })
        .fail(function(jqXHR, textStatus){
            Swal.close();
            Swal.fire({
                title: 'Error',
                text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        });
    }

    function toggleConfirm(type,user){
        var txt = type?"¿Está seguro que desea desactivar este usuario?":"¿Está seguro que desea reactivar este usuario?";
        Swal.fire({
            title: 'Confirmación',
            text: txt,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                toggleUser(user);
            }
        });
    }

    $(document).ready(function () {
        userTable = $('#users').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "ajax": {
                "url": "<?=base_url()?>Users/getUserTable",
                "type": "POST",
                "data": function (d) {

                }
            },
            "columnDefs": [
                { 
                    "targets": 0,
                    "visible": false
                },
                {
                    "targets": [3,4],
                    "orderable": false
                },
                {
                    className: "text-center",
                    targets: "_all"
                },
                {
                    targets: [3,4],
                    width: "10%"
                }
            ]
        });
        $('#userModal').modal({
            backdrop: 'static',
            show: false
        });
    });
</script>