<div class="container">
    <div class="row justify-content-center">
        <div class="col-8 text-center">
            <h1>Noticias</h1>
        </div>
    </div>
    <div class="row justify-content-right">
        <div class="form-group col">
            <button class="btn btn-outline-primary btnBootstrap float-right" onclick="openNewsModal();"><i class="fas fa-fw fa-plus"></i>Añadir Noticia</button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col">
            <div class="table-responsive">
                <table id="newsTable" class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Contenido</th>
                            <th>Fecha</th>
                            <th>Estado</th>
                            <th>Activar/Desactivar</th>
                            <th>Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal" id="newsModal" tabindex="-1" role="dialog" aria-labelledby="newsModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newsModalLabel">Noticia</h5>
                <button type="button" class="close btnBootstrap" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="news_form" action="" method="post" enctype="multipart/form-data">
                    <div class="form-row">
                        <input id="new" type="text" class="hidden" value=0 hidden>
                        <div class="form-group col-12">
                            <div class="form-group">
                                <label for="title">*Título</label>
                                <input class="form-control" id="title" name="title" type="text" placeholder="Título de la noticia" >
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <div class="form-group">
                                <label for="content">*Descripción</label>
                                <textarea class="form-control" id="content" name="content" rows="5" placeholder="Descripción de la noticia" ></textarea>
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <div class="form-group">
                                <label for="date">*Fecha</label>
                                <input class="form-control" id="date" name="date" type="date" >
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <div class="form-group">
                                <label for="image">Imagen</label>
                                <input class="form-control" id="image" name="image" type="file" >
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <div class="form-group">
                                <label for="video">*Video</label>
                                <input class="form-control" id="video" name="video" rows="5" placeholder="Ingrese url del video" >
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary btnBootstrap" data-dismiss="modal">Cerrar</button>
                <button type="button" onclick="saveNews()" class="btn btn-outline-success btnBootstrap">Guardar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var newsTable;
    function openNewsModal(){
        $("#new").val(0);
        $('#news_form')[0].reset();
        $('#newsModal').modal('show');
    }

    $("#news_form").on("submit",function (e) { 
        e.preventDefault();
        return false;
    });

    function openEditModal(news){
        Swal.fire({
            title: 'Cargando ...',
            text: 'Espere un momento por favor.',
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        $.ajax({
            type: "post",
            url: "<?=base_url()?>News/getNewsInfo",
            data: {
                idNews: news
            }
        })
        .done(function (res) {
            res = JSON.parse(res);
            Swal.close();
            if(res){
                $('#news_form')[0].reset();
                $("#new").val(news);
                $("#title").val(res.title);
                $("#content").val(res.content);
                $("#date").val(res.date);
                $("#video").val(res.video);
                $('#newsModal').modal('show');
            }
        })
        .fail(function(jqXHR, textStatus){
            Swal.close();
            Swal.fire({
                title: 'Error',
                text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        });
    }

    function toggleNews(news = 0){
        Swal.close();
        Swal.fire({
            title: 'Cargando ...',
            text: 'Espere un momento por favor.',
            onBeforeOpen: () => {
                Swal.showLoading();
            },
        });
        $.ajax({
            type: "post",
            url: "<?=base_url()?>News/toggleNews",
            data: {
                idNews: news
            }
        })
        .done(function (res) {
            res = JSON.parse(res);
            Swal.close();
            if(res){
                Swal.fire({
                    title: 'Éxito',
                    text: 'Cambio guardado correctamente',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                });
            }else{
                Swal.fire({
                    title: 'Error',
                    text: 'Ha ocurrido un error inesperado al guardar el cambio',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            }
            newsTable.draw();
        })
        .fail(function(jqXHR, textStatus){
            Swal.close();
            Swal.fire({
                title: 'Error',
                text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        });
    }

    function toggleConfirm(type,news){
        var txt = type?"¿Está seguro que desea desactivar este usuario?":"¿Está seguro que desea reactivar este usuario?";
        Swal.fire({
            title: 'Confirmación',
            text: txt,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                toggleNews(news);
            }
        });
    }

    function saveNews() {
        var form = $("#news_form")[0];
        var formData = new FormData(form);
        if($("#title").val() != '' && $("#content").val() != '' && $("#date").val() != '' && $("#video").val() != ''){
            formData.append("idNews", $("#new").val());
            Swal.close();
            Swal.fire({
                title: 'Cargando ...',
                text: 'Espere un momento por favor.',
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
            });
            $.ajax({
                type: "post",
                url: "<?=base_url()?>News/saveNews",
                data: formData,
                enctype: 'multipart/form-data',
                contentType: false,       // The content type used when sending data to the server.
                cache: false,             // To unable request pages to be cached
                processData:false        // To send DOMDocument or non processed data file it is set to false
            })
            .done(function (data) {
                data = JSON.parse(data);
                Swal.close();
                if(data.status == 1){
                    if(data.files.length > 0){
                        var uploadFiles = true;
                        $.each(data.files, function (i, v) { 
                            if(v.status != 1){
                                uploadFiles = false;
                            }
                        });
                        if(uploadFiles === false){
                            Swal.fire({
                                title: 'Advertencia',
                                text: 'Información guardada, pero se encontraron problemas al guardar los archivos subidos.',
                                icon: 'warning',
                                confirmButtonText: 'Ok'
                            });
                        }else{
                            Swal.fire({
                                title: 'Éxito',
                                text: 'Información guardada correctamente',
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }else{
                        Swal.fire({
                            title: 'Advertencia',
                            text: 'Información guardada, pero se encontraron problemas al guardar los archivos subidos.',
                            icon: 'warning',
                            confirmButtonText: 'Ok'
                        });
                    }
                    newsTable.draw();
                    $('#newsModal').modal('hide');
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: 'Ha ocurrido un error al guardar la información',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                }
            })
            .fail(function(jqXHR, textStatus){
                Swal.close();
                Swal.fire({
                    title: 'Error',
                    text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
                console.log(jqXHR);
                console.log(textStatus);
            });
        }else{
            Swal.fire({
                title: 'Error!',
                text: 'Necesita completar los todos los campos marcados con *',
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        }
    }

    $(document).ready(function () {
        newsTable = $('#newsTable').DataTable({
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "order": [[ 0, 'desc' ]],
            "ajax": {
                "url": "<?=base_url()?>News/getNewsTable",
                "type": "POST",
                "data": function (d) {

                }
            },
            "columnDefs": [
                { 
                    "targets": 0,
                    "visible": false
                },
                {
                    "targets": [3,4],
                    "orderable": false
                },
                {
                    className: "text-center",
                    targets: "_all"
                },
                {
                    targets: [3,4],
                    width: "10%"
                }
            ]
        });

        $('#newsModal').modal({
            backdrop: 'static',
            show: false
        });

    });
</script>