<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PANEL DE ADMINISTRACION CONCIMAZ | CONTRALORIA CIUDADANA MAZATLAN</title>
    <link rel="icon" href="../favicon.ico">

    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,700|Anton" rel="stylesheet">
    <link rel="stylesheet" href="../fonts/icomoon/style.css">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    <link rel="stylesheet" href="../css/jquery-ui.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" href="../css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="../css/aos.css">
    <link rel="stylesheet" href="../css/style.css">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <style>
        ul.panel .nav-item.active{
            background-color: #e9ecef33;
        }
        label{
            font-weight: bold;
        }
        button.swal2-confirm.swal2-styled{
            border-top-color: transparent !important;
            border-bottom-color: transparent !important;
            border-left-color: rgb(158, 96, 180) !important;
            border-right-color: rgb(158, 96, 180) !important;
        }
        img{
            height: 100px !important;
        }
    </style>
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="./">PANEL DE ADMINISTRACION</a>
        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <!-- Navbar -->
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="sidebar navbar-nav panel">
            <li class="nav-item active">
                <a class="nav-link" href="./">
                    <i class="fas fa-fw fa-users"></i>
                    <span>Login</span>
                </a>
            </li>
        </ul>
        <div id="content-wrapper">
            <!-- /.container-fluid -->
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="./">Panel</a>
                    </li>
                    <li class="breadcrumb-item active">Login</li>
                </ol>
            </div>
            <!-- /.container-fluid -->
            <!-- /.container-fluid -->
            <div id="panel_content" class="container-fluid">
                <div id="loginModal" class="modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Login de acceso</h5>
                            </div>
                            <div class="modal-body">
                                <div class="row text-center">
                                    <div class="col">
                                        <img class="img-fluid" src="../images/logo_concimaz.png" alt="Logo Concimaz">
                                    </div>
                                </div>
                                <form autocomplete="off">
                                    <div class="form-group">
                                        <label for="panelUser">Email</label>
                                        <input type="email" class="form-control" id="panelUser" placeholder="Ingrese su email" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="panelPass">Contraseña</label>
                                        <input type="password" class="form-control" id="panelPass" placeholder="Ingrese la contraseña" autocomplete="off">
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button id='loginBtn' onclick="login()" class="btn btn-primary">Entrar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
            <!-- Sticky Footer -->
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright ©2019 Todos los derechos reservados | CONCIMAZ</span>
                    </div>
                </div>
            </footer>
            <!-- Sticky Footer -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Bootstrap core JavaScript-->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <!-- Custom scripts for all pages-->
    <script src="../js/sb-admin.min.js"></script>
    <script type="text/javascript">
        function login(){
            if($("#panelUser").val().trim() == '' && $("#panelPass").val().trim() == ''){
                Swal.fire({
                    title: 'Error!',
                    text: 'Es necesario ingresar un usuario y contraseña para continuar',
                    icon: 'warning',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            Swal.fire({
                title: 'Cargando ...',
                text: 'Espere un momento por favor.',
                onBeforeOpen: () => {
                    Swal.showLoading();
                },
            });
            $.ajax({
                type: "post",
                url: "<?=base_url()?>Login/login",
                data: {
                    user: $("#panelUser").val(),
                    pass: $("#panelPass").val()
                }/*,
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true*/
            })
            .done(function (res) {
                res = JSON.parse(res);
                Swal.close();
                if(res){
                    location.reload(true);
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: 'Usuario y/o contraseña incorrectos',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                }
            })
            .fail(function(jqXHR, textStatus){
                Swal.close();
                Swal.fire({
                    title: 'Error!',
                    text: 'Ha ocurrido un error, favor de intentarlo de nuevo',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                });
            });
        }
        $(document).ready(function () {
            $('#loginModal').modal({
                keyboard: false,
                backdrop: 'static'
            });
            $("#loginModal").modal('show');
            $('#loginModal').on('keypress', function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $('#loginBtn').click();   
                }
            });
        });
    </script>
</body>
</html>