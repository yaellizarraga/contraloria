<?php
include('config.php');
if(isset($_SESSION['SERVICE'])) {
  $addRoute = "../../";
} else if(isset($_SESSION['NEWS']) || isset($_SESSION['EVENTS'])) {
  $addRoute = "../../../";
} else {
  $addRoute = "";
}
?>
<div class="site-mobile-menu site-navbar-target">
  <div class="site-mobile-menu-header">
    <div class="site-mobile-menu-close mt-3">
      <span class="icon-close2 js-menu-toggle"></span>
    </div>
  </div>
  <div class="site-mobile-menu-body"></div>
</div>
<div class="top-bar">
  <div class="container">
    <div class="d-flex justify-content-between">
      <div class="d-flex flex-fill justify-content-start">
        <a href="https://www.facebook.com/contraloriaciudadanamzt/" class="pl-0 pr-3" target="_blank"><span class="icon-facebook"></span></a>
        <a href="https://twitter.com/ContraloraCiud2" target="_blank" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
        <a href="https://www.instagram.com/concimaz/" target="_blank" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
        <a href="https://www.youtube.com/channel/UCjTuIaNvzpYsSS19TLRTQpg" target="_blank" class="pl-3 pr-3"><span class="icon-youtube"></span></a>
      </div>
      <div class="d-flex flex-fill justify-content-end" id="UserNameContainer">
        <?php if(isset($_SESSION['sessionInit']) && $_SESSION['sessionInit']=== true){ ?>
          <input type="hidden" id="hiddenIdUser" value="<?php echo $_SESSION['idUser'];?>">
          <a href="#"><strong><?php echo 'Ciudadano: '.$_SESSION['username'];?></strong></a>
          <div class="dividerTop"></div>
          <a href="<?php echo BASE_URL.'logout.php';?>">Salir</a>
        <?php } else{ ?>
        <div class="login-icon">
          <a id="login" href="#">
            Login
            <span class="icon-user"></span>
          </a>
        </div>
        <div class="register-icon">
          <a id="registro" href="#">
            Registrate
            <span class="icon-user-plus"></span>
          </a>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
<header class="site-navbar js-sticky-header site-navbar-target" role="banner">
  <div class="container">
    <?php if (isset($_SESSION['SERVICE'])) { ?>
      <div class="flex-row">
        <div class="d-flex justify-content-end align-items-center">
          <a href="<?php echo BASE_URL.'?toservices=true'; ?>">
            <img src="https://img.icons8.com/office/40/000000/circled-chevron-left.png" class="mr-3">
            Volver al inicio
          </a>
        </div>
      </div>
    <?php }else if(isset($_SESSION['NEWS'])){ ?>
      <div class="flex-row">
        <div class="d-flex justify-content-end align-items-center">
          <a href="<?php echo BASE_URL.'?tonews=true'; ?>">
            <img src="https://img.icons8.com/office/40/000000/circled-chevron-left.png" class="mr-3">
            Volver al inicio
          </a>
        </div>
      </div>
    <?php }else if(isset($_SESSION['EVENTS'])){ ?>
      <div class="flex-row">
        <div class="d-flex justify-content-end align-items-center">
          <a href="<?php echo BASE_URL.'?toevents=true'; ?>">
            <img src="https://img.icons8.com/office/40/000000/circled-chevron-left.png" class="mr-3">
            Volver al inicio
          </a>
        </div>
      </div>
    <?php }else{ ?>
      <div class="row align-items-center position-relative">
          <div class="site-logo">
            <a href="<?php echo BASE_URL;?>" class="text-black"><span class="text-primary-color"><img class="logo-contraloria" src="<?php echo $addRoute;?>images/logo.png"/></a>
          </div>
            <nav class="site-navigation text-center ml-auto" role="navigation">
              <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block" id="main_menu">
                <li><a href="#home-section" class="nav-link">Inicio</a></li>
                <li><a href="#about-section" class="nav-link">Quienes Somos</a></li>
                <li><a id="service" href="#services-section" class="nav-link">Servicios</a></li>
                <li><a href="#blog-section" class="nav-link" id="eventos-link" onclick="showTab('eventos')">Eventos</a></li>
                <li><a href="#blog-section" class="nav-link" id="noticias-link" onclick="showTab('noticias')">Noticias</a></li>
                <li><a href="https://concimazgames.game.blog/" target="_blank" class="nav-link">Blog</a></li>
                <li><a href="#contact-section" class="nav-link">Contacto</a></li>
                <li class="dropdown d-none d-sm-none d-md-block" style="display: inline-block !important;">
                  <a class="dropdown-toggle nav-link" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Mas
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a target="_blank" class="dropdown-item nav-link" href="http://www.diputados.gob.mx/LeyesBiblio/index.htm">Biblioteca de Leyes Federales</a>
                    <a target="_blank" class="dropdown-item nav-link" href="#">Biblioteca de Leyes de Sinaloa</a>
                    <!--<div class="dropdown-divider"></div>
                    <a class="dropdown-item nav-link" href="#">Something else here</a>-->
                  </div>
                </li>
                <!--Show mas dropdown content on mobile START-->
                <li><hr class="d-block d-sm-none d-none d-sm-block d-md-none" /></li>
                <li><a href="#" class="nav-link d-sm-block d-md-none d-lg-none">Mas</a></li>
                <li><hr class="d-block d-sm-none d-none d-sm-block d-md-none" /></li>
                <li><a class="nav-link d-sm-block d-md-none d-lg-none" href="http://www.diputados.gob.mx/LeyesBiblio/index.htm" target="_blank">Biblioteca de Leyes Federales</a></li>
                <li><a class="nav-link d-sm-block d-md-none d-lg-none" href="#" target="_blank">Biblioteca de Leyes de Sinaloa</a></li>
                <!--Show mas dropdown content on mobile START-->
                <li><hr class="d-block d-sm-none d-none d-sm-block d-md-none" /></li>
                <li><a href="#" class="nav-link d-sm-block d-md-none d-lg-none">Acerca de</a></li>
                <li><hr class="d-block d-sm-none d-none d-sm-block d-md-none" /></li>
                <li><a href="javascript:openAbout('mision')" class="nav-link d-block d-sm-none d-none d-sm-block d-md-none">Misión</a></li>
                <li><a href="javascript:openAbout('vision')" class="nav-link d-block d-sm-none d-none d-sm-block d-md-none">Visión</a></li>
                <li><a href="javascript:openAbout('valores')" class="nav-link d-block d-sm-none d-none d-sm-block d-md-none">Valores</a></li>
                <li><a href="javascript:openAbout('organigrama')" class="nav-link d-block d-sm-none d-none d-sm-block d-md-none">Organigrama</a></li>
                <li><a href="javascript:openAbout('comisiones')" class="nav-link d-block d-sm-none d-none d-sm-block d-md-none">Comisiones</a></li>
              </ul>
            </nav>
           <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>
      </div>
    <?php } ?>
  </div>

</header>
